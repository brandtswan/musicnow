# MusicNow

Group Members (GitLab ID, EID): 
- Darshan Bhatta: darshanbhatta, db43787
- David Qi: davidqi1, dq993
- Mason Eastman: meastman555, mae3255
- Michael Cao: michael-c-123, mwc944
- Brandt Swanson: brandtswan, bas4932

Phase 4 Git SHA: 7418dca699e640fd7b4a444f993a2354b1d98940

Phase 1 Leader: David Qi
Phase 2 Leader: Mason Eastman
Phase 3 Leader: Darshan Bhatta
Phase 4 Leader: Brandt Swanson

GitLab Pipelines: https://gitlab.com/brandtswan/musicnow/-/pipelines

Website: https://www.music-now.me/ https://music-now.me/

API: https://api.music-now.me

Estimated and Actual Completion time for each member
- Format: Estimate/Actual P1 | P2 | P3 | P4

- Darshan Bhatta 5/6 | 10/12 | 5/3 | 3/2 |

- David Qi 7/6.5 | 15/16 | 10/10 | 5/5 |

- Mason Eastman 8/6.5 | 20/35 | 10/11.5 | 8/5 |

- Michael Cao 4/5 | 15/10 | 10/20 | 2/4 |

- Brandt Swanson 3/4 | 20/12 | 5/10 | 8/5 |

Comments:

