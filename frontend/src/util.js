export function toTimeString(millis) {
  const sec = Math.floor(millis / 1000);
  return `${Math.floor(sec / 60)}:${sec % 60 < 10 ? "0" : ""}${sec % 60}`;
}

export function toNumberString(x) {
  if (x < 1000) {
    return x.toString();
  } else if (x < 10000) {
    const hundreds = x % 1000;
    const thousands = Math.floor(x / 1000);
    return `${thousands},${hundreds}`;
  } else if (x < 1000000) {
    return `${Math.floor(x / 1000)}K`;
  } else if (x < 1000000000) {
    return `${Math.floor(x / 1000000)}M`;
  }
  return `${Math.floor(x / 1000000000)}B`;
}

export const camelToDisplay = (text) => {
  const result = text.replace(/([A-Z])/g, " $1");
  return result.charAt(0).toUpperCase() + result.slice(1);
};

export const getSnippet = (query, text) => {
  if (query && text) {
    let chars = [];
    for (var i = 0; i < query.length; i++) {
      chars.push(query.charAt(i));
    }
    const caseInsensitive = chars
      .map((c) => `[${c.toLowerCase()}${c.toUpperCase()}]`)
      .join("");
    const regex = `(?:(?:\\S+\\s){1,5}|^)${caseInsensitive}\\S{0,3}\\s+(?:\\S+\\s){0,5}`;
    const result = text.match(new RegExp(regex));
    if (result) {
      return result[0];
    }
  }
  return null;
};

export const highlight = (query, text) => {
  if (!query || !text) {
    return null;
  }
  const found = text.toLowerCase().indexOf(query.toLowerCase());
  if (found === -1) {
    return null;
  }
  return (
    <>
      {text.slice(0, found)}
      <span className="highlight">
        {text.slice(found, found + query.length)}
      </span>
      {text.slice(found + query.length)}
    </>
  );
};