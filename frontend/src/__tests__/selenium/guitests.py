import unittest
from time import sleep
from selenium import webdriver

# make sure to enter the docker image before running these tests or else you'll get errors

# switch when dev changes are integrated into production
# HOST_NAME = 'https://dev.d3seyrdswp8bfy.amplifyapp.com'
HOST_NAME = 'https://www.music-now.me'

DEFAULT_ELEMENTS_PER_PAGE = 30
DRIVER_WAIT_TIME = 10

# defines unit test class for selenium test cases to quickly and efficiently test a lot
# the driver gets the website every test because different pages are tested
# sleeps and refreshes when loading a page because rarely something with the API loading was messing tests up (but refreshing seems to fix)

# testing format based off Relocccate https://gitlab.com/bhaver/relocccate/-/blob/master/frontend/src/Tests/Selenium/GUITests.py
class SeleniumTests(unittest.TestCase):

    # initializes driver data for unit tests
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        # unsure what these two do, but after a lot of errors the internet told me I needed them
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        # tell selenium there is no graphics our monitor display for this driver
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.driver = webdriver.Chrome(options=chrome_options)
        # forces driver to wait so that data on all pages can be loaded before each test is run
        self.driver.implicitly_wait(DRIVER_WAIT_TIME)

    # cleans up driver data
    def tearDown(self):
        self.driver.quit()

    # make sure the html root element is present and rendered
    def test_root_element(self):
        self.driver.get(HOST_NAME)
        sleep(3)
        self.driver.refresh()
        result = self.driver.find_element_by_css_selector('#root')
        self.assertNotEqual(result, None)

    # title on splash page rendering
    def test_splash_page(self):
        self.driver.get(HOST_NAME)
        sleep(3)
        self.driver.refresh()
        # if the h1 title on splash changes, this would need to change too
        title = self.driver.find_element_by_tag_name('h1')
        self.assertEqual(title.text, 'MusicNow')

    # about us on splash link can be followed
    def test_about_link(self):
        self.driver.get(HOST_NAME)
        link = self.driver.find_element_by_link_text('About Us')
        link.click()
        sleep(3)
        self.driver.refresh()
        # if the h1 on about us page title changes, this would need to change too
        title = self.driver.find_element_by_tag_name('h1')
        self.assertEqual(title.text, 'MusicNow')

    # there are two links to songs, artists, and ablums on splash page (one on main page and one on navbar) -- they point to the same places
    # songs link on splash page goes to correct page
    def test_songs_link(self):
        self.driver.get(HOST_NAME)
        link = self.driver.find_element_by_link_text('Songs')
        link.click()
        sleep(3)
        self.driver.refresh()
        # if the h1 on songs page title changes, this would need to change too
        title = self.driver.find_element_by_tag_name('h1')
        self.assertEqual(title.text, 'Songs')

    # artists link on splash page goes to correct page
    def test_artists_link(self):
        self.driver.get(HOST_NAME)
        link = self.driver.find_element_by_link_text('Artists')
        link.click()
        sleep(3)
        self.driver.refresh()
        # if the h1 on songs page title changes, this would need to change too
        title = self.driver.find_element_by_tag_name('h1')
        self.assertEqual(title.text, 'Artists')

    # albums link on splash page goes to correct page
    def test_albums_link(self):
        self.driver.get(HOST_NAME)
        link = self.driver.find_element_by_link_text('Albums')
        link.click()
        sleep(3)
        self.driver.refresh()
        # if the h1 on songs page title changes, this would need to change too
        title = self.driver.find_element_by_tag_name('h1')
        self.assertEqual(title.text, 'Albums')

    # about page renders the correct number of cards
    def test_about_cards(self):
        self.driver.get(f'{HOST_NAME}/about')
        sleep(3)
        self.driver.refresh()
        cards = self.driver.find_elements_by_id('about-card')
        self.assertEqual(len(cards), 9)

    # songs page has correct number of table entries
    # table view is default view
    def test_songs_list(self):
        self.driver.get(f'{HOST_NAME}/songs')
        sleep(3)
        self.driver.refresh()
        table = self.driver.find_elements_by_class_name('click-row')
        self.assertEqual(len(table), DEFAULT_ELEMENTS_PER_PAGE)

    # songs page has correct number of cards
    def test_songs_grid(self):
        self.driver.get(f'{HOST_NAME}/songs')
        sleep(3)
        self.driver.refresh()
        view_button = self.driver.find_element_by_id('grid-view-button')
        view_button.click()
        grid_cards = self.driver.find_elements_by_class_name('card')
        self.assertEqual(len(grid_cards), DEFAULT_ELEMENTS_PER_PAGE)

    # artists page has correct number of table entries
    def test_artists_list(self):
        self.driver.get(f'{HOST_NAME}/artists')
        sleep(3)
        self.driver.refresh()
        view_button = self.driver.find_element_by_id('list-view-button')
        view_button.click()
        table = self.driver.find_elements_by_class_name('click-row')
        self.assertEqual(len(table), DEFAULT_ELEMENTS_PER_PAGE)

    # artist page has correct number of cards
    # grid view is default view
    def test_artists_grid(self):
        self.driver.get(f'{HOST_NAME}/artists')
        sleep(3)
        self.driver.refresh()
        grid_cards = self.driver.find_elements_by_class_name('card')
        self.assertEqual(len(grid_cards), DEFAULT_ELEMENTS_PER_PAGE)

    # albums page has correct number of table entries
    def test_albums_table(self):
        self.driver.get(f'{HOST_NAME}/albums')
        sleep(3)
        self.driver.refresh()
        view_button = self.driver.find_element_by_id('list-view-button')
        view_button.click()
        table = self.driver.find_elements_by_class_name('click-row')
        self.assertEqual(len(table), DEFAULT_ELEMENTS_PER_PAGE)

    # albums page has correct number of cards
    # grid view is default view
    def test_albums_grid(self):
        self.driver.get(f'{HOST_NAME}/albums')
        sleep(3)
        self.driver.refresh()
        grid_cards = self.driver.find_elements_by_class_name('card')
        self.assertEqual(len(grid_cards), DEFAULT_ELEMENTS_PER_PAGE)

    # song instance shows correct info
    def test_song_instance(self):
        self.driver.get(f'{HOST_NAME}/songs/0')
        sleep(3)
        self.driver.refresh()
        # if the h5 on songs instance page changes, this would need to change too
        name = self.driver.find_elements_by_tag_name('h5')[0]
        self.assertEqual(name.text, 'Everything I Am')

    # artist instance shows correct info
    def test_artist_instance(self):
        self.driver.get(f'{HOST_NAME}/artists/0')
        sleep(3)
        self.driver.refresh()
        # if the h5 on songs instance page changes, this would need to change too
        name = self.driver.find_elements_by_tag_name('h5')[0]
        self.assertEqual(name.text, 'Kanye West')

    # albums instance shows correct info
    def test_album_instance(self):
        self.driver.get(f'{HOST_NAME}/albums/0')
        sleep(3)
        self.driver.refresh()
        # if the h5 on songs instance page changes, this would need to change too
        name = self.driver.find_elements_by_tag_name('h5')[0]
        self.assertEqual(name.text, 'Graduation')

    # not found page gets shown when needed
    def test_not_found(self):
        # mocking a not found call
        self.driver.get(f'{HOST_NAME}/songs/100000')
        sleep(3)
        self.driver.refresh()
        # song-instance-background is also reused on the not found page
        not_found = self.driver.find_element_by_class_name('song-instance-background')
        self.assertNotEqual(not_found, None)

    # the backend tests ensure that the calls for searching, sorting, and filtering return the correct number of results
    # jest ensures that the searching and filtering inputs are present
    # however, the sorting input and highlighting involve the GUI more so they are put here instead

    def test_songs_sorting_input(self):
        self.driver.get(f'{HOST_NAME}/songs')
        sleep(3)
        self.driver.refresh();
        # sort only possible on table view, so manually switch to it
        self.driver.find_element_by_id('list-view-button').click()
        sort_toggles = self.driver.find_elements_by_id('sort');
        self.assertNotEqual(sort_toggles, None)

    def test_artists_sorting_input(self):
        self.driver.get(f'{HOST_NAME}/artists')
        sleep(3)
        self.driver.refresh();
        # sort only possible on table view, so manually switch to it
        self.driver.find_element_by_id('list-view-button').click()
        sort_toggles = self.driver.find_elements_by_id('sort');
        self.assertNotEqual(sort_toggles, None)

    def test_albums_sorting_input(self):
        self.driver.get(f'{HOST_NAME}/albums')
        sleep(3)
        self.driver.refresh();
        # sort only possible on table view, so manually switch to it
        self.driver.find_element_by_id('list-view-button').click()
        sort_toggles = self.driver.find_elements_by_id('sort');
        self.assertNotEqual(sort_toggles, None)

    # highlighting looks for the span used to highlight, and if found means the algo worked
    # if the css class name -- highlight -- changes, these tests would need to change as well

    def test_songs_search_highlighting(self):
        self.driver.get(f'{HOST_NAME}/songs')
        sleep(3)
        self.driver.refresh();
        search_bar = self.driver.find_element_by_id('search')
        search_bar.send_keys('drake')
        sleep(3)
        highlight_span = self.driver.find_elements_by_css_selector('highlight')
        self.assertNotEqual(highlight_span, None)

    def test_artists_search_highlighting(self):
        self.driver.get(f'{HOST_NAME}/artists')
        sleep(3)
        self.driver.refresh();
        search_bar = self.driver.find_element_by_id('search')
        search_bar.send_keys('john')
        sleep(3)
        highlight_span = self.driver.find_elements_by_css_selector('highlight')
        self.assertNotEqual(highlight_span, None)

    def test_albums_search_highlighting(self):
        self.driver.get(f'{HOST_NAME}/albums')
        sleep(3)
        self.driver.refresh();
        search_bar = self.driver.find_element_by_id('search')
        search_bar.send_keys('abbey road')
        sleep(3)
        highlight_span = self.driver.find_elements_by_css_selector('highlight')
        self.assertNotEqual(highlight_span, None)

    def test_sitewide_search_highlighting(self):
        self.driver.get(f'{HOST_NAME}')
        sleep(3)
        self.driver.refresh();
        search_bar = self.driver.find_element_by_id('search')
        search_bar.send_keys("taylor swift")
        sleep(3)
        highlight_span = self.driver.find_elements_by_css_selector('highlight')
        self.assertNotEqual(highlight_span, None)

# entry point for unit test framework
if __name__ == '__main__':
    unittest.main(warnings='ignore')
