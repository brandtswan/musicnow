import { render } from "@testing-library/react";

import AlbumInstance from "../../views/AlbumInstance";

it('album instance page renders without crashing', () => {
    render(<AlbumInstance match={{params: {id: 0}}} />);
});
