import { render } from "@testing-library/react";

import SongInstance from "../../views/SongInstance";

it('artist instance page renders without crashing', () => {
    render(<SongInstance match={{params: {id: 0}}} />);
});
