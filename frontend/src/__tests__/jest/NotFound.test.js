import { render } from "@testing-library/react";

import NotFound from "../../components/NotFound";

it('not found renders without crashing', () => {
    render(<NotFound />);
});
