import { render, screen } from "@testing-library/react";

import SongsList from "../../views/lists/SongsList";

it('songslist page renders without crashing', () => {
    render(<SongsList />);
});

it('songslist page has filter input', () => {
    render(<SongsList />);
    const filterDropdown = screen.getByTestId('filter');
    expect(filterDropdown).toBeInTheDocument();
});

it('songslist page has search input', () => {
    render(<SongsList />);
    const searchBar = screen.getByTestId('search');
    expect(searchBar).toBeInTheDocument();
});

//sorting test moved to selenium
