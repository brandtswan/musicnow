import { render } from "@testing-library/react";

import ArtistInstance from "../../views/ArtistInstance";

it('artist instance page renders without crashing', () => {
    render(<ArtistInstance match={{params: {id: 0}}} />);
});
