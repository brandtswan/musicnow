import { render, screen } from "@testing-library/react";

import ArtistsList from "../../views/lists/ArtistsList";

it('artistslist page renders without crashing', () => {
    render(<ArtistsList />);
});

it('artistslist page has filter input', () => {
    render(<ArtistsList />);
    const filterDropdown = screen.getByTestId('filter');
    expect(filterDropdown).toBeInTheDocument();
});

it('artistslist page has search input', () => {
    render(<ArtistsList />);
    const searchBar = screen.getByTestId('search');
    expect(searchBar).toBeInTheDocument();
});

//sorting test moved to selenium
