import { render, screen } from "@testing-library/react";

import AlbumsList from "../../views/lists/AlbumsList";

it('albumlist page renders without crashing', () => {
    render(<AlbumsList />);
});

it('albumslist page has filter input', () => {
    render(<AlbumsList />);
    const filterDropdown = screen.getByTestId('filter');
    expect(filterDropdown).toBeInTheDocument();
});

it('albumslist page has search input', () => {
    render(<AlbumsList />);
    const searchBar = screen.getByTestId('search');
    expect(searchBar).toBeInTheDocument();
});

//sorting test moved to selenium
