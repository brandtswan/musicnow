const PROD_HOSTNAME = "https://api.music-now.me";
const LOCAL_HOSTNAME = "http://localhost:5000";

// change this to the API you want the backend to point to
const HOSTNAME = PROD_HOSTNAME;

const cache = new Map();

export async function getArtist(id) {
  const artist = await makeRequest(`artists/${id}`);
  return artist?.artist || null;
}

export async function getSong(id) {
  const song = await makeRequest(`songs/${id}`);
  return song?.song || null;
}

export async function getAlbum(id) {
  const album = await makeRequest(`albums/${id}`);
  return album?.album || null;
}

export async function getArtists(
  limit,
  offset,
  sort,
  ascending,
  searchQuery,
  filterAge,
  filterListeners,
  filterFollowers
) {
  // const key = `artists-${limit}-${offset}`;
  // if (cache.has(key)) return cache.get(key);

  let sortString = sort ? `&sort=${sort}` : "";
  let directionString = sort ? (ascending ? "&order=ascending" : "&order=descending") : "";
  let searchString =
    searchQuery && searchQuery !== "" ? `&search=${searchQuery}` : "";
  let filterAgeString = filterAge ? `&filterAge=${filterAge[0]},${filterAge[1]}` : "";
  let filterListenersString = filterListeners ? `&filterLastFMListeners=${filterListeners[0]},${filterListeners[1]}` : "";
  let filterFollowersString = filterFollowers ? `&filterSpotifyFollowers=${filterFollowers[0]},${filterFollowers[1]}` : "";
  const artists = await makeRequest(
    "artists",
    `?limit=${limit}&offset=${offset}${sortString}${directionString}${searchString}${filterAgeString}${filterListenersString}${filterFollowersString}`
  );
  // if (artists) cache.set(key, artists);
  return artists || null;
}

export async function getSongs(
  limit,
  offset,
  sort,
  ascending,
  searchQuery,
  filterDuration
) {
  // const key = `songs-${limit}-${offset}`;
  // if (cache.has(key)) return cache.get(key);

  let sortString = sort ? `&sort=${sort}` : "";
  let directionString = sort ? (ascending ? "&order=ascending" : "&order=descending") : "";
  let searchString =
    searchQuery && searchQuery !== "" ? `&search=${searchQuery}` : "";
  let filterDurationString = filterDuration ? `&filterDuration=${filterDuration[0]},${filterDuration[1]}` : "";

  const songs = await makeRequest(
    "songs",
    `?limit=${limit}&offset=${offset}${sortString}${directionString}${searchString}${filterDurationString}`
  );

  // if (songs) cache.set(key, songs);
  return songs || null;
}

export async function getAlbums(
  limit,
  offset,
  sort,
  ascending,
  searchQuery,
  filterSongCount,
  filterDuration,
  filterListeners
) {
  // const key = `albums-${limit}-${offset}`;
  // if (cache.has(key)) return cache.get(key);

  let sortString = sort ? `&sort=${sort}` : "";
  let directionString = sort ? (ascending ? "&order=ascending" : "&order=descending") : "";
  let searchString =
    searchQuery && searchQuery !== "" ? `&search=${searchQuery}` : "";
  let filterSongCountString = filterSongCount ? `&filterSongCount=${filterSongCount[0]},${filterSongCount[1]}` : "";
  let filterDurationString = filterDuration ? `&filterDuration=${filterDuration[0]},${filterDuration[1]}` : "";
  let filterListenersString = filterListeners ? `&filterLastFMListeners=${filterListeners[0]},${filterListeners[1]}` : "";

  const albums = await makeRequest(
    "albums",
    `?limit=${limit}&offset=${offset}${sortString}${directionString}${searchString}${filterSongCountString}${filterDurationString}${filterListenersString}`
  );

  // if (albums) cache.set(key, albums);
  return albums || null;
}

/**
 * Wrapper function to easily make requests to our api
 * @param {String} path pathname
 * @param {String?} query query string
 * @returns
 */
async function makeRequest(path, query) {
  try {
    const res = await fetch(`${HOSTNAME}/${path}${query ? query : ""}`, {
      method: "GET",
    });
    const data = await res.json();
    return data;
  } catch (err) {
    console.log(`[API ERROR] path: ${path}`, err);
    return null;
  }
}
