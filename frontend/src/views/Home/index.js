import React, { useState } from "react";
import background from "../../images/newsplashpage.jpg";
import {
  Container,
  Row,
  Col,
  Card,
  CardTitle,
  CardLink,
} from "reactstrap";
import { BsPersonFill, BsMusicNoteBeamed } from "react-icons/bs";
import { MdLibraryMusic, MdSearch } from "react-icons/md";

import "./index.css";

const Home = (props) => {
  return (
    <div>
      <div className="splash-body">
        <img src={background} alt="" className="splash-background" />
      </div>
      <div className="splash-main-wrapper">
        <h1 className="display-3 splash-main-logo">MusicNow</h1>
        <Container>
          <Row>
            <Col
              className="display-5"
              xs="12"
              md="4"
              style={{ marginBottom: "5%" }}
            >
              <Card>
                <CardLink href="/songs" className="splash-link">
                  <BsMusicNoteBeamed className="splash-icon" />
                  <CardTitle>Songs</CardTitle>
                </CardLink>
              </Card>
            </Col>

            <Col
              className="display-5"
              xs="12"
              md="4"
              style={{ marginBottom: "5%" }}
            >
              <Card>
                <CardLink href="/artists" className="splash-link">
                  <BsPersonFill className="splash-icon" />
                  <CardTitle>Artists</CardTitle>
                </CardLink>
              </Card>
            </Col>

            <Col
              className="display-5"
              xs="12"
              md="4"
              style={{ marginBottom: "5%" }}
            >
              <Card>
                <CardLink href="/albums" className="splash-link">
                  <MdLibraryMusic className="splash-icon" />
                  <CardTitle>Albums</CardTitle>
                </CardLink>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
};

export default Home;
