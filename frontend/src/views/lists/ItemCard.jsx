import React from "react";
import {
  Card,
  CardTitle,
  CardText,
  CardImg,
  CardFooter,
  CardBody,
} from "reactstrap";

const wrapAnchor = (link, child) => (link ? <a href={link}>{child}</a> : child);

const ItemCard = ({
  title,
  subtitle,
  image,
  alt,
  link = null,
  subtitleLink = null,
  details = null,
}) => {
  const createDetails = () => {
    if (!details || details.length === 0) return undefined;

    return details.map((item) => {
      if (Array.isArray(item) && item.length > 0) {
        const bullets = [];
        for (const bullet of item) {
          bullets.push(bullet);
          bullets.push(" \u2022 ");
        }
        bullets.pop();
        return <div>{bullets.join("")}</div>;
      } else {
        return <div>{item}</div>;
      }
    });
  };

  return (
    <Card
      style={{ marginBottom: "6px"}}
      overflow="hidden"
      color="dark"
    >
      <CardBody>
        {wrapAnchor(
          link,
          <CardTitle tag="h5" className="white">
            {title}
          </CardTitle>
        )}
        {wrapAnchor(
          subtitleLink,
          <CardText className="white">{subtitle}</CardText>
        )}
        {wrapAnchor(
          link,
          <CardImg
            style={{
              objectFit: "cover",
              width: "auto",
              height: "150px",
              marginTop: "6px",
              marginBottom: "auto",
            }}
            bottom
            src={image}
            alt={alt}
          />
        )}
      </CardBody>
      <CardFooter className="white">
        {createDetails()}
      </CardFooter>
    </Card>
  );
};

export default ItemCard;
