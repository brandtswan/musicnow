import React from "react";
import { Button, ButtonGroup } from "reactstrap";
import { MdViewModule, MdList } from "react-icons/md";

const ViewSwitch = ({isListView, onChanged}) => {
  return (
    <ButtonGroup className="float-end">
      <Button
        color={isListView ? "primary" : "secondary"}
        onClick={() => {
          onChanged(true);
        }}
        id="list-view-button"
      >
        <MdList />
      </Button>
      <Button
        color={isListView ? "secondary" : "primary"}
        onClick={() => {
          onChanged(false);
        }}
        id="grid-view-button"
      >
        <MdViewModule />
      </Button>
    </ButtonGroup>
  );
};

export default ViewSwitch;
