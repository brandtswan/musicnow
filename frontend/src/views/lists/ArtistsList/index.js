import React, { useState, useEffect } from "react";
import {
  Button,
  Col,
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Input,
  InputGroup,
  Jumbotron,
  Row,
  UncontrolledCollapse,
  UncontrolledDropdown,
  UncontrolledTooltip,
} from "reactstrap";
import {
  MdFilterList,
  MdSearch,
  MdArrowDownward,
  MdArrowUpward,
} from "react-icons/md";

import "../style.css";

import CardView from "../CardView";
import ItemCard from "../ItemCard";
import TableView from "../TableView";
import ListPagination from "../ListPagination";
import { getArtists } from "../../../apis";
import Loading from "../../../components/Loading";
import ViewSwitch from "../ViewSwitch";
import { highlight, toNumberString } from "../../../util";
import LastFmImage from "../../../images/lastfm.png";
import SpotifyImage from "../../../images/spotify.png";

const ageFilters = [
  { label: " < 18", range: [0, 18] },
  { label: "18 - 21", range: [18, 22] },
  { label: "22 - 29", range: [22, 30] },
  { label: "30 - 39", range: [30, 40] },
  { label: "20 - 49", range: [40, 50] },
  { label: "25 - 59", range: [50, 60] },
  { label: "25 - 69", range: [60, 70] },
  { label: "> 70", range: [70, 999999999999] },
];

const listenerFilters = [
  { label: "< 10K", range: [0, 10000] },
  { label: "10K - 50K", range: [10000, 50000] },
  { label: "50K - 100K", range: [50000, 100000] },
  { label: "100K - 500K", range: [100000, 500000] },
  { label: "500K - 1M", range: [500000, 1000000] },
  { label: "1M - 2M", range: [1000000, 2000000] },
  { label: "2M - 3M", range: [2000000, 3000000] },
  { label: "> 3M", range: [3000000, 999999999999] },
];

const followerFilters = [
  { label: "< 10K", range: [0, 10000] },
  { label: "10K - 50K", range: [10000, 50000] },
  { label: "50K - 100K", range: [50000, 100000] },
  { label: "100K - 500K", range: [100000, 500000] },
  { label: "500K - 1M", range: [500000, 1000000] },
  { label: "1M - 5M", range: [1000000, 5000000] },
  { label: "5M - 10M", range: [5000000, 10000000] },
  { label: "> 10M", range: [10000000, 999999999999] },
];

const ArtistsList = () => {
  const [currentPage, setCurrentPage] = useState(0);
  const [perPage, setPerPage] = useState(30);
  const [isListView, setListView] = useState(false);

  const [page, setPage] = useState();
  const [totalRecords, setTotalRecords] = useState(0);

  const [tempQuery, setTempQuery] = useState("");

  const [searchQuery, setSearchQuery] = useState(undefined);
  const [sort, setSort] = useState(undefined);
  const [isAscending, setAscending] = useState(true);
  const [ageFilter, setAgeFilter] = useState(undefined);
  const [listenerFilter, setListenerFilter] = useState(undefined);
  const [followerFilter, setFollowerFilter] = useState(undefined);

  useEffect(() => {
    setPage(undefined);
    async function fetchArtists() {
      const start = currentPage * perPage;
      const _artists = await getArtists(
        perPage,
        start,
        sort,
        isAscending,
        searchQuery,
        ageFilter ? ageFilter.range : undefined,
        listenerFilter ? listenerFilter.range : undefined,
        followerFilter ? followerFilter.range : undefined
      );
      setPage(_artists.page);
      setTotalRecords(_artists.totalRecords);
    }
    fetchArtists();
  }, [
    currentPage,
    perPage,
    sort,
    isAscending,
    searchQuery,
    ageFilter,
    listenerFilter,
    followerFilter,
  ]);

  function createRow(artist) {
    return (
      <tr
        key={artist.id}
        onClick={() => {
          window.location.href = `artists/${artist.id}`;
        }}
        className="click-row"
      >
        <th scope="row">
          {highlight(searchQuery, artist.name) ?? artist.name}
        </th>
        <td>{highlight(searchQuery, artist.label) ?? artist.label}</td>
        <td>{artist.age === 0 ? "-" : artist.age}</td>
        <td>{toNumberString(artist.lastFmListeners)}</td>
        <td>{toNumberString(artist.spotifyFollowers)}</td>
      </tr>
    );
  }

  function createCard(item) {
    return (
      <ItemCard
        key={item.id}
        title={highlight(searchQuery, item.name) ?? item.name}
        subtitle={highlight(searchQuery, item.label) ?? item.label}
        image={item.image}
        alt={`image of ${item.name}`}
        link={`artists/${item.id}`}
        details={[
          <div>
            <span id={"lastfm" + item.id}>
              <img src={LastFmImage} width="20px" height="20px" />
              {toNumberString(item.lastFmListeners)}
            </span>
            {" \u2022 "}
            <span id={"spotify" + item.id}>
              <img src={SpotifyImage} width="20px" height="20px" />
              {toNumberString(item.spotifyFollowers)}
            </span>
            <UncontrolledTooltip target={"lastfm" + item.id}>
              last.fm Listeners
            </UncontrolledTooltip>
            <UncontrolledTooltip target={"spotify" + item.id}>
              Spotify Followers
            </UncontrolledTooltip>
          </div>,
          item.age === 0 ? <></> : `Age ${item.age}`,
        ]}
      />
    );
  }

  const buttonSortToggle = (attribute) => {
    let newAscending = sort !== attribute || !isAscending;
    setSort(attribute);
    setAscending(newAscending);
  };

  return (
    <div>
      <Jumbotron className="white">
        <h1 className="display-3">Artists</h1>
      </Jumbotron>

      <Container style={{ maxWidth: "100%" }}>
        <Row>
          <Col xs={12}>
            <InputGroup>
              <Input
                id="search"
                data-testid="search"
                name="search"
                type="search"
                placeholder="Search"
                onChange={(e) => {
                  let query = e.target.value;
                  setTempQuery(query);
                  if (query === "" && searchQuery) {
                    setSearchQuery(undefined);
                  }
                }}
                onKeyDown={(e) => {
                  let query = e.target.value.trim();
                  if (e.key === "Enter" && query !== "") {
                    setSearchQuery(query);
                  }
                }}
              />
              <Button
                onClick={() => {
                  setSearchQuery(tempQuery);
                }}
              >
                <MdSearch />
              </Button>
            </InputGroup>
          </Col>
        </Row>
        <Row>
          <Col xs="2">
            <Button
              id="filterToggler"
              data-testid="filter"
              className="float-start spaced"
            >
              Filter
              <MdFilterList style={{ marginLeft: "4px" }} />
            </Button>
          </Col>
          <Col xs="10">{/* Show filters here */}</Col>
        </Row>
        <Row>
          <Col xs="12">
            <UncontrolledCollapse toggler="#filterToggler">
              <UncontrolledDropdown>
                <DropdownToggle caret className="float-start spaced">
                  {`Age${
                    ageFilter ? ": " + ageFilter.label : ""
                  }`}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => setAgeFilter(undefined)}>
                    none
                  </DropdownItem>
                  {ageFilters.map((item, i) => {
                    return (
                      <DropdownItem
                        key={i}
                        onClick={() => setAgeFilter(item)}
                      >
                        {item.label}
                      </DropdownItem>
                    );
                  })}
                </DropdownMenu>
              </UncontrolledDropdown>
              <UncontrolledDropdown>
                <DropdownToggle caret className="float-start spaced">
                  {`last.fm Listeners${
                    listenerFilter ? ": " + listenerFilter.label : ""
                  }`}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => setListenerFilter(undefined)}>
                    none
                  </DropdownItem>
                  {listenerFilters.map((item, i) => {
                    return (
                      <DropdownItem
                        key={i}
                        onClick={() => setListenerFilter(item)}
                      >
                        {item.label}
                      </DropdownItem>
                    );
                  })}
                </DropdownMenu>
              </UncontrolledDropdown>
              <UncontrolledDropdown>
                <DropdownToggle caret className="float-start spaced">
                  {`Spotify Followers${
                    followerFilter ? ": " + followerFilter.label : ""
                  }`}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => setFollowerFilter(undefined)}>
                    none
                  </DropdownItem>
                  {followerFilters.map((item, i) => {
                    return (
                      <DropdownItem
                        key={i}
                        onClick={() => setFollowerFilter(item)}
                      >
                        {item.label}
                      </DropdownItem>
                    );
                  })}
                </DropdownMenu>
              </UncontrolledDropdown>
            </UncontrolledCollapse>
          </Col>
        </Row>
        {!isListView && (
          <>
            <div
              className="float-start white"
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              Sort by:
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("name")}
              >
                Name
                {sort === "name" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("label")}
              >
                Label
                {sort === "label" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("age")}
              >
                Age
                {sort === "age" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("last_fm_listeners")}
              >
                last.fm Listeners
                {sort === "last_fm_listeners" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("spotify_followers")}
              >
                Spotify Followers
                {sort === "spotify_followers" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
            </div>
            <br />
            <br />
          </>
        )}
      </Container>

      {page ? (
        <>
          <Container style={{ maxWidth: "100%" }}>
            <Row>
              <Col xs="10">
                <ListPagination
                  page={currentPage}
                  perPage={perPage}
                  totalEntries={totalRecords}
                  onPageChanged={setCurrentPage}
                  onPerPageChanged={setPerPage}
                />
              </Col>
              <Col xs="2">
                <ViewSwitch isListView={isListView} onChanged={setListView} />
              </Col>
            </Row>
          </Container>
          {isListView ? (
            <TableView
              headers={[
                { attribute: "name", title: "Name", width: "40%" },
                { attribute: "label", title: "Label" },
                { attribute: "age", title: "Age", width: "20%" },
                { attribute: "last_fm_listeners", title: "last.fm Listeners" },
                { attribute: "spotify_followers", title: "Spotify Followers" },
              ]}
              id="sort"
              sort={sort}
              ascending={isAscending}
              onSortChanged={(newSort, newIsAscending) => {
                setSort(newSort);
                setAscending(newIsAscending);
              }}
            >
              {page.map(createRow)}
            </TableView>
          ) : (
            <CardView data={page} createCard={createCard} />
          )}
        </>
      ) : (
        <Loading />
      )}
    </div>
  );
};

export default ArtistsList;
