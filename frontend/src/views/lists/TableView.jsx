import React from "react";
import { Table } from "reactstrap";
import { MdArrowUpward, MdArrowDownward } from "react-icons/md";

const TableView = ({ headers, children, sort, ascending, onSortChanged }) => {
  return (
    <Table responsive dark hover>
      <thead>
        <tr>
          {headers.map((header) => {
            let style = {};
            let onClick = undefined;
            let arrow = undefined;
            style.width = header.width;

            if (!header.noPointer) {
              style.cursor = "pointer";
              onClick = () => {
                let newAscending = true;
                if (sort) {
                  newAscending = sort !== header.attribute || !ascending;
                }
                onSortChanged(header.attribute, newAscending);
              };
              if (sort === header.attribute) {
                arrow = ascending ? <MdArrowUpward /> : <MdArrowDownward />;
              }
            }
            return (
              <th style={style} onClick={onClick}>
                {header.title} {arrow}
              </th>
            );
          })}
        </tr>
      </thead>
      <tbody>{children}</tbody>
    </Table>
  );
};

export default TableView;
