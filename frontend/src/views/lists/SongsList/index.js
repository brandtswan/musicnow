import React, { useState, useEffect } from "react";
import {
  Badge,
  Button,
  Col,
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Input,
  InputGroup,
  Jumbotron,
  Row,
  UncontrolledCollapse,
  UncontrolledDropdown,
  UncontrolledTooltip,
} from "reactstrap";
import { getSongs } from "../../../apis";
import Loading from "../../../components/Loading";
import { highlight, toTimeString } from "../../../util";
import CardView from "../CardView";
import ItemCard from "../ItemCard";
import ListPagination from "../ListPagination";
import {
  MdFilterList,
  MdSearch,
  MdArrowUpward,
  MdArrowDownward,
} from "react-icons/md";

import "../style.css";
import TableView from "../TableView";
import ViewSwitch from "../ViewSwitch";

const durationFilters = [
  { label: " < 1:00", range: [0, 60] },
  { label: "1:00-1:59", range: [60, 120] },
  { label: "2:00-2:59", range: [120, 180] },
  { label: "3:00-3:59", range: [180, 240] },
  { label: "4:00-4:59", range: [240, 300] },
  { label: "5:00-5:59", range: [300, 360] },
  { label: " > 6:00", range: [360, 999999999999] },
];

const releasedFilters = [
  { label: "before 1900", range: [0, 1900] },
  { label: "1900-1930", range: [1900, 1930] },
  { label: "1930-1960", range: [1930, 1960] },
  { label: "1960s", range: [1960, 1970] },
  { label: "1970s", range: [1970, 1980] },
  { label: "1980s", range: [1980, 1990] },
  { label: "1990s", range: [1990, 2000] },
  { label: "2000s", range: [2000, 2010] },
  { label: "2010s", range: [2010, 2020] },
  { label: "2020s", range: [2020, 2030] },
];

const Songs = (props) => {
  const [currentPage, setCurrentPage] = useState(0);
  const [perPage, setPerPage] = useState(30);
  const [isListView, setListView] = useState(true);

  const [page, setPage] = useState();
  const [totalRecords, setTotalRecords] = useState(0);

  const [tempQuery, setTempQuery] = useState("");

  const [searchQuery, setSearchQuery] = useState(undefined);
  const [sort, setSort] = useState(undefined);
  const [isAscending, setAscending] = useState(true);
  const [durationFilter, setDurationFilter] = useState(undefined);
  const [releasedFilter, setReleasedFilter] = useState(undefined);

  useEffect(() => {
    async function fetchSongs() {
      setPage(undefined);
      const start = currentPage * perPage;
      const _songs = await getSongs(
        perPage,
        start,
        sort,
        isAscending,
        searchQuery,
        durationFilter ? durationFilter.range.map((x) => x * 1000) : undefined
      );
      setPage(_songs.page);
      setTotalRecords(_songs.totalRecords);
    }
    fetchSongs();
  }, [currentPage, perPage, sort, isAscending, searchQuery, durationFilter]);

  function createRow(song) {
    const duration = toTimeString(song.duration);
    const linkTo = (route) => {
      window.location.href = route;
    };
    return (
      <tr key={song.id} className="click-row">
        <th scope="row" onClick={() => linkTo(`songs/${song.id}`)}>
          {highlight(searchQuery, song.name) ?? song.name}
        </th>
        <td>
          <a href={`artists/${song.artistId}`}>
            {highlight(searchQuery, song.artistName) ?? song.artistName}
          </a>
        </td>
        <td onClick={() => linkTo(`songs/${song.id}`)}>
          {song.genres.slice(0, 4).map((genre, i) => (
            <Badge key={i} style={{ background: "grey", margin: "0px 2px" }}>
              {genre}
            </Badge>
          ))}
        </td>
        <td onClick={() => linkTo(`songs/${song.id}`)}>{duration}</td>
        <td onClick={() => linkTo(`songs/${song.id}`)}>{song.releaseDate}</td>
      </tr>
    );
  }

  function createCard(item) {
    return (
      <ItemCard
        key={item.id}
        title={highlight(searchQuery, item.name) ?? item.name}
        subtitle={highlight(searchQuery, item.artistName) ?? item.artistName}
        image={item.image}
        alt={`cover art of ${item.name}`}
        link={`songs/${item.id}`}
        subtitleLink={`artists/${item.artistId}`}
        details={[
          <div>
            <span>{toTimeString(item.duration) + " \u2022 "}</span>
            <span id={"released" + item.id}>
              {item.releaseDate.slice(0, 4)}
            </span>
            <UncontrolledTooltip
              target={"released" + item.id}
              placement="right"
            >
              {`Released ${item.releaseDate}`}
            </UncontrolledTooltip>
          </div>,
          ,
          <div>
            {item.genres.slice(0, 3).map((genre, i) => (
              <Badge key={i} style={{ background: "grey", margin: "0px 2px" }}>
                {genre}
              </Badge>
            ))}
          </div>,
        ]}
      />
    );
  }

  const buttonSortToggle = (attribute) => {
    let newAscending = sort !== attribute || !isAscending;
    setSort(attribute);
    setAscending(newAscending);
  };

  return (
    <div>
      <Jumbotron style={{ color: "white" }}>
        <h1 className="display-3">Songs</h1>
      </Jumbotron>
      <Container style={{ maxWidth: "100%" }}>
        <Row>
          <Col xs={12}>
            <InputGroup>
              <Input
                id="search"
                data-testid="search"
                name="search"
                type="search"
                placeholder="Search"
                onChange={(e) => {
                  let query = e.target.value;
                  setTempQuery(query);
                  if (query === "" && searchQuery) {
                    setSearchQuery(undefined);
                  }
                }}
                onKeyDown={(e) => {
                  let query = e.target.value.trim();
                  if (e.key === "Enter" && query !== "") {
                    setSearchQuery(query);
                  }
                }}
              />
              <Button
                onClick={() => {
                  setSearchQuery(tempQuery);
                }}
              >
                <MdSearch />
              </Button>
            </InputGroup>
          </Col>
        </Row>
        <Row>
          <Col xs="2">
            <Button
              id="filterToggler"
              data-testid="filter"
              className="float-start spaced"
            >
              Filter
              <MdFilterList style={{ marginLeft: "4px" }} />
            </Button>
          </Col>
        </Row>
        <Row>
          <Col xs="12">
            <UncontrolledCollapse toggler="#filterToggler">
              <UncontrolledDropdown>
                <DropdownToggle caret className="float-start spaced">
                  {`Duration${
                    durationFilter ? ": " + durationFilter.label : ""
                  }`}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => setDurationFilter(undefined)}>
                    none
                  </DropdownItem>
                  {durationFilters.map((item, i) => {
                    return (
                      <DropdownItem
                        key={i}
                        onClick={() => setDurationFilter(item)}
                      >
                        {item.label}
                      </DropdownItem>
                    );
                  })}
                </DropdownMenu>
              </UncontrolledDropdown>
            </UncontrolledCollapse>
          </Col>
        </Row>
        {!isListView && (
          <>
            <div
              className="float-start white"
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              Sort by:
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("name")}
              >
                Title
                {sort === "name" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("artist")}
              >
                Artist
                {sort === "artist" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("duration")}
              >
                Duration
                {sort === "duration" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("date")}
              >
                Release Date
                {sort === "date" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
            </div>
            <br />
            <br />
          </>
        )}
      </Container>
      {page ? (
        <>
          <Container style={{ maxWidth: "100%" }}>
            <Row>
              <Col xs="10">
                <ListPagination
                  page={currentPage}
                  perPage={perPage}
                  totalEntries={totalRecords}
                  onPageChanged={setCurrentPage}
                  onPerPageChanged={setPerPage}
                />
              </Col>
              <Col xs="2">
                <ViewSwitch isListView={isListView} onChanged={setListView} />
              </Col>
            </Row>
          </Container>
          {isListView ? (
            <TableView
              headers={[
                { attribute: "name", title: "Title", width: "40%" },
                { attribute: "artist", title: "Artist" },
                {
                  attribute: "genres",
                  title: "Genres",
                  width: "20%",
                  noPointer: true,
                },
                { attribute: "duration", title: "Duration" },
                { attribute: "date", title: "Released" },
              ]}
              id="sort"
              sort={sort}
              ascending={isAscending}
              onSortChanged={(newSort, newIsAscending) => {
                setSort(newSort);
                setAscending(newIsAscending);
              }}
            >
              {page.map(createRow)}
            </TableView>
          ) : (
            <CardView data={page} createCard={createCard} />
          )}
        </>
      ) : (
        <Loading />
      )}
    </div>
  );
};

export default Songs;
