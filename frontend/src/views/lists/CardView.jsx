import React from "react";
import { Col, Container, Row } from "reactstrap";

const CardView = ({ data, createCard }) => {
  const cards = data.map(createCard);
  return (
    <Container fluid={true}>
      <Row xl="5" lg="4" md="3" sm="2" xs="2" className="flex-grow">
        {cards.map((card, i) => (
          <Col key={i}>{card}</Col>
        ))}
      </Row>
    </Container>
  );
};

export default CardView;
