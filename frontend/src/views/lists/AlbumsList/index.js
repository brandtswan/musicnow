import React, { useState, useEffect } from "react";
import {
  Button,
  Col,
  Container,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Input,
  InputGroup,
  Jumbotron,
  Row,
  UncontrolledCollapse,
  UncontrolledDropdown,
  UncontrolledTooltip,
} from "reactstrap";
import {
  MdFilterList,
  MdSearch,
  MdArrowDownward,
  MdArrowUpward,
} from "react-icons/md";

import { getAlbums } from "../../../apis";
import Loading from "../../../components/Loading";
import { highlight, toNumberString, toTimeString } from "../../../util";
import CardView from "../CardView";
import ItemCard from "../ItemCard";
import ListPagination from "../ListPagination";
import LastFmImage from "../../../images/lastfm.png";

import "../style.css";
import TableView from "../TableView";
import ViewSwitch from "../ViewSwitch";

const songCountFilters = [
  { label: " < 5", range: [0, 5] },
  { label: "5 - 9", range: [5, 10] },
  { label: "10 - 14", range: [10, 15] },
  { label: "15 - 19", range: [15, 20] },
  { label: "20 - 24", range: [20, 25] },
  { label: "25 - 29", range: [25, 30] },
  { label: "> 30", range: [30, 999999999999] },
];

const durationFilters = [
  { label: " < 15 min", range: [0, 15 * 60] },
  { label: "15 - 29 min", range: [15, 30] },
  { label: "30 - 44 min", range: [30, 45] },
  { label: "45 - 59 min", range: [45, 60] },
  { label: "1:00 - 1:30", range: [60, 90] },
  { label: "1:30 - 2:00", range: [90, 120] },
  { label: "2:00 - 3:00", range: [120, 180] },
  { label: "> 3:00", range: [180, 999999999999] },
];

const listenerFilters = [
  { label: "< 10K", range: [0, 10000] },
  { label: "10K - 50K", range: [10000, 50000] },
  { label: "50K - 100K", range: [50000, 100000] },
  { label: "100K - 500K", range: [100000, 500000] },
  { label: "500K - 1M", range: [500000, 1000000] },
  { label: "1M - 2M", range: [1000000, 2000000] },
  { label: "2M - 3M", range: [2000000, 3000000] },
  { label: "> 3M", range: [3000000, 999999999999] },
];

const releasedFilters = [
  { label: "before 1900", range: [0, 1899] },
  { label: "1900-1930", range: [1900, 1929] },
  { label: "1930-1960", range: [1930, 1959] },
  { label: "1960s", range: [1960, 1969] },
  { label: "1970s", range: [1970, 1979] },
  { label: "1980s", range: [1980, 1989] },
  { label: "1990s", range: [1990, 1999] },
  { label: "2000s", range: [2000, 2009] },
  { label: "2010s", range: [2010, 2019] },
  { label: "2020s", range: [2020, 2029] },
];
const AlbumsList = (props) => {
  const [currentPage, setCurrentPage] = useState(0);
  const [perPage, setPerPage] = useState(30);
  const [isListView, setListView] = useState(false);

  const [page, setPage] = useState();
  const [totalRecords, setTotalRecords] = useState(0);

  const [tempQuery, setTempQuery] = useState("");

  const [searchQuery, setSearchQuery] = useState(undefined);
  const [sort, setSort] = useState(undefined);
  const [isAscending, setAscending] = useState(true);
  const [songCountFilter, setSongCountFilter] = useState(undefined);
  const [listenerFilter, setListenerFilter] = useState(undefined);
  const [durationFilter, setDurationFilter] = useState(undefined);
  const [releasedFilter, setReleasedFilter] = useState(undefined);

  useEffect(() => {
    async function fetchAlbums() {
      const start = currentPage * perPage;
      setPage(undefined);
      const _albums = await getAlbums(
        perPage,
        start,
        sort,
        isAscending,
        searchQuery,
        songCountFilter ? songCountFilter.range : undefined,
        durationFilter
          ? durationFilter.range.map((x) => x * 60 * 1000)
          : undefined,
        listenerFilter ? listenerFilter.range : undefined
      );
      setPage(_albums.page);
      setTotalRecords(_albums.totalRecords);
    }
    fetchAlbums();
  }, [
    currentPage,
    perPage,
    sort,
    isAscending,
    searchQuery,
    songCountFilter,
    durationFilter,
    listenerFilter,
  ]);

  function createRow(album) {
    const duration = toTimeString(album.totalDuration);

    return (
      <tr
        key={album.id}
        onClick={() => {
          window.location.href = `albums/${album.id}`;
        }}
        className="click-row"
      >
        <th scope="row">{highlight(searchQuery, album.name) ?? album.name}</th>
        <td>
          <a href={`artists/${album.artistId}`}>
            {highlight(searchQuery, album.artistName) ?? album.artistName}
          </a>
        </td>
        <td>{album.songCount}</td>
        <td>{duration}</td>
        <td>{toNumberString(album.lastFmListeners)}</td>
        <td>{album.releaseDate}</td>
      </tr>
    );
  }

  function createCard(item) {
    return (
      <ItemCard
        key={item.id}
        title={highlight(searchQuery, item.name) ?? item.name}
        subtitle={highlight(searchQuery, item.artistName) ?? item.artistName}
        image={item.image}
        alt={`cover art of ${item.name}`}
        link={`albums/${item.id}`}
        subtitleLink={`artists/${item.artistId}`}
        details={[
          [
            `${item.songCount} single${item.songCount > 1 ? "s" : ""}`,
            toTimeString(item.totalDuration),
          ],
          <div>
            <span id={"released" + item.id}>
              {item.releaseDate.slice(0, 4)}
            </span>
            <UncontrolledTooltip
              target={"released" + item.id}
              placement="right"
            >
              {`Released ${item.releaseDate}`}
            </UncontrolledTooltip>
          </div>,
          <div>
            <span id={"lastfm" + item.id}>
              <img src={LastFmImage} width="20px" height="20px" />
              {toNumberString(item.lastFmListeners)}
            </span>
            <UncontrolledTooltip target={"lastfm" + item.id} placement="right">
              last.fm Listeners
            </UncontrolledTooltip>
          </div>,
        ]}
        minHeight="360px"
      />
    );
  }

  const buttonSortToggle = (attribute) => {
    let newAscending = sort !== attribute || !isAscending;
    setSort(attribute);
    setAscending(newAscending);
  };

  return (
    <div>
      <Jumbotron className="white">
        <h1 className="display-3">Albums</h1>
      </Jumbotron>
      <Container style={{ maxWidth: "100%" }}>
        <Row>
          <Col xs={12}>
            <InputGroup>
              <Input
                id="search"
                data-testid="search"
                name="search"
                type="search"
                placeholder="Search"
                onChange={(e) => {
                  let query = e.target.value;
                  setTempQuery(query);
                  if (query === "" && searchQuery) {
                    setSearchQuery(undefined);
                  }
                }}
                onKeyDown={(e) => {
                  let query = e.target.value.trim();
                  if (e.key === "Enter" && query !== "") {
                    setSearchQuery(query);
                  }
                }}
              />
              <Button
                onClick={() => {
                  setSearchQuery(tempQuery);
                }}
              >
                <MdSearch />
              </Button>
            </InputGroup>
          </Col>
        </Row>
        <Row>
          <Col xs="2">
            <Button
              id="filterToggler"
              data-testid="filter"
              className="float-start spaced"
            >
              Filter
              <MdFilterList style={{ marginLeft: "4px" }} />
            </Button>
          </Col>
          <Col xs="10">{/* Show filters here */}</Col>
        </Row>
        <Row>
          <Col xs="12">
            <UncontrolledCollapse toggler="#filterToggler">
              <UncontrolledDropdown>
                <DropdownToggle caret className="float-start spaced">
                  {`Num. Singles${
                    songCountFilter ? ": " + songCountFilter.label : ""
                  }`}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => setSongCountFilter(undefined)}>
                    none
                  </DropdownItem>
                  {songCountFilters.map((item, i) => {
                    return (
                      <DropdownItem
                        key={i}
                        onClick={() => setSongCountFilter(item)}
                      >
                        {item.label}
                      </DropdownItem>
                    );
                  })}
                </DropdownMenu>
              </UncontrolledDropdown>
              <UncontrolledDropdown>
                <DropdownToggle caret className="float-start spaced">
                  {`last.fm Listeners${
                    listenerFilter ? ": " + listenerFilter.label : ""
                  }`}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => setListenerFilter(undefined)}>
                    none
                  </DropdownItem>
                  {listenerFilters.map((item, i) => {
                    return (
                      <DropdownItem
                        key={i}
                        onClick={() => setListenerFilter(item)}
                      >
                        {item.label}
                      </DropdownItem>
                    );
                  })}
                </DropdownMenu>
              </UncontrolledDropdown>
              <UncontrolledDropdown>
                <DropdownToggle caret className="float-start spaced">
                  {`Duration${
                    durationFilter ? ": " + durationFilter.label : ""
                  }`}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem onClick={() => setDurationFilter(undefined)}>
                    none
                  </DropdownItem>
                  {durationFilters.map((item, i) => {
                    return (
                      <DropdownItem
                        key={i}
                        onClick={() => setDurationFilter(item)}
                      >
                        {item.label}
                      </DropdownItem>
                    );
                  })}
                </DropdownMenu>
              </UncontrolledDropdown>
            </UncontrolledCollapse>
          </Col>
        </Row>
        {!isListView && (
          <>
            <div
              className="float-start white"
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              Sort by:
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("name")}
              >
                Name
                {sort === "name" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("artist_name")}
              >
                Artist Name
                {sort === "artist_name" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("song_count")}
              >
                Num. Singles
                {sort === "song_count" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("total_duration")}
              >
                Total Duration
                {sort === "total_duration" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("last_fm_listeners")}
              >
                last.fm Listeners
                {sort === "last_fm_listeners" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
              <Button
                className="float-start spaced"
                onClick={() => buttonSortToggle("release_date")}
              >
                Release Date
                {sort === "release_date" &&
                  (isAscending ? <MdArrowUpward /> : <MdArrowDownward />)}
              </Button>
            </div>
            <br />
            <br />
          </>
        )}
      </Container>
      {page ? (
        <>
          <Container style={{ maxWidth: "100%" }}>
            <Row>
              <Col xs="10">
                <ListPagination
                  page={currentPage}
                  perPage={perPage}
                  totalEntries={totalRecords}
                  onPageChanged={setCurrentPage}
                  onPerPageChanged={setPerPage}
                />
              </Col>
              <Col xs="2">
                <ViewSwitch isListView={isListView} onChanged={setListView} />
              </Col>
            </Row>
          </Container>
          {isListView ? (
            <TableView
              headers={[
                { attribute: "name", title: "Name", width: "40%" },
                { attribute: "artist_name", title: "Artist" },
                { attribute: "song_count", title: "Num. Singles" },
                { attribute: "total_duration", title: "Total Duration" },
                { attribute: "last_fm_listeners", title: "last.fm Listeners" },
                { attribute: "release_date", title: "Release Date" },
              ]}
              id="sort"
              sort={sort}
              ascending={isAscending}
              onSortChanged={(newSort, newIsAscending) => {
                setSort(newSort);
                setAscending(newIsAscending);
              }}
            >
              {page.map(createRow)}
            </TableView>
          ) : (
            <CardView data={page} createCard={createCard} />
          )}
        </>
      ) : (
        <Loading />
      )}
    </div>
  );
};

export default AlbumsList;
