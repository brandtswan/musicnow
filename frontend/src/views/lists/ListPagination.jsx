import React, { useEffect, useState } from "react";
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Pagination,
  PaginationItem,
  PaginationLink,
} from "reactstrap";

const ListPagination = ({
  page,
  perPage,
  totalEntries,
  onPageChanged,
  onPerPageChanged,
}) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const thresh = 700;
  const [isCondensed, setCondensed] = useState(window.innerWidth < thresh);

  useEffect(() => {
    const handleResize = () => setCondensed(window.innerWidth < thresh);
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  });

  let end = Math.ceil(totalEntries / perPage) - 1;
  if (end < 0) {
    end = 0;
  }

  let left = Math.max(0, page - 2);
  let right = Math.min(end, page + 2);

  if (left === 0) {
    right = Math.min(end, 4);
  } else if (right === end) {
    left = Math.max(0, right - 4);
  }

  let pageButtons = [];
  if (isCondensed) {
    pageButtons = (
      <PaginationItem active={true}>
        <PaginationLink href="#" onClick={() => onPageChanged(page)}>
          {page + 1}
        </PaginationLink>
      </PaginationItem>
    );
  } else {
    for (let i = left; i <= right; i++) {
      pageButtons.push(
        <PaginationItem key={i} active={i === page}>
          <PaginationLink href="#" onClick={() => onPageChanged(i)}>
            {i + 1}
          </PaginationLink>
        </PaginationItem>
      );
    }
  }

  return (
    <div
      className="float-start"
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: "5px",
      }}
    >
      <Pagination style={{ marginBottom: "0px" }}>
        <PaginationItem disabled={page === 0}>
          <PaginationLink
            style={{ backgroundColor: page === 0 ? "#AAA" : "#FFF" }}
            href="#"
            onClick={() => onPageChanged(0)}
          >
            &lt;&lt;
          </PaginationLink>
        </PaginationItem>
        <PaginationItem disabled={page === 0}>
          <PaginationLink
            style={{ backgroundColor: page === 0 ? "#AAA" : "#FFF" }}
            href="#"
            onClick={() => onPageChanged(page - 1)}
          >
            &lt;
          </PaginationLink>
        </PaginationItem>
        {pageButtons}
        <PaginationItem disabled={page === end}>
          <PaginationLink
            href="#"
            style={{ backgroundColor: page === end ? "#AAA" : "#FFF" }}
            onClick={() => onPageChanged(page + 1)}
          >
            &gt;
          </PaginationLink>
        </PaginationItem>
        <PaginationItem disabled={page === end}>
          <PaginationLink
            href="#"
            style={{ backgroundColor: page === end ? "#AAA" : "#FFF" }}
            onClick={() => onPageChanged(end)}
          >
            &gt;&gt;
          </PaginationLink>
        </PaginationItem>
      </Pagination>
      <Dropdown
        style={{ marginLeft: "5px" }}
        isOpen={dropdownOpen}
        toggle={() => setDropdownOpen((prevState) => !prevState)}
      >
        <DropdownToggle caret>{perPage}</DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={() => onPerPageChanged(15)}>15</DropdownItem>
          <DropdownItem onClick={() => onPerPageChanged(30)}>30</DropdownItem>
          <DropdownItem onClick={() => onPerPageChanged(60)}>60</DropdownItem>
          <DropdownItem onClick={() => onPerPageChanged(75)}>75</DropdownItem>
          <DropdownItem onClick={() => onPerPageChanged(90)}>90</DropdownItem>
        </DropdownMenu>
      </Dropdown>
      <div className="white" style={{ marginLeft: "5px" }}>
        {isCondensed ? `/${totalEntries}` : ` out of ${totalEntries} items`}
      </div>
    </div>
  );
};

export default ListPagination;
