import React, { useState, useMemo, useEffect } from "react";
import { useLocation } from "react-router-dom";
import {
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
} from "reactstrap";
import { getAlbums, getArtists, getSongs } from "../../apis";
import Loading from "../../components/Loading";
import { camelToDisplay, getSnippet, highlight } from "../../util";
import "./style.css";

function useUrlParams() {
  const { search } = useLocation();

  return useMemo(() => new URLSearchParams(search), [search]);
}
const SearchLanding = (props) => {
  const [activeTab, setActiveTab] = useState("songs");
  const [results, setResults] = useState(undefined);

  let urlParams = useUrlParams();

  useEffect(() => {
    async function fetchModel(apiRequestFunction) {
      const result = await apiRequestFunction(
        // TODO paginate
        30,
        0,
        undefined,
        undefined,
        urlParams.get("q")
      );
      return result.page;
    }

    (async () => {
      const allResults = await Promise.all([
        fetchModel(getSongs),
        fetchModel(getArtists),
        fetchModel(getAlbums),
      ]);
      setResults({
        songs: allResults[0],
        artists: allResults[1],
        albums: allResults[2],
      });
    })();
  }, []);

  if (!results) {
    return <Loading />;
  }

  return (
    <>
      <Nav tabs>
        <NavItem>
          <NavLink
            className={activeTab === "songs" ? "active" : ""}
            onClick={() => setActiveTab("songs")}
          >
            Songs
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={activeTab === "artists" ? "active" : ""}
            onClick={() => setActiveTab("artists")}
          >
            Artists
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={activeTab === "albums" ? "active" : ""}
            onClick={() => setActiveTab("albums")}
          >
            Albums
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={activeTab}>
        <TabPane tabId="songs">
          {results.songs.map((item) =>
            createResult(
              item,
              urlParams.get("q"),
              `songs/${item.id}`,
              "name",
              "artistName",
              ["albumName"],
              ["description", "lyrics"]
            )
          )}
        </TabPane>
        <TabPane tabId="artists">
          {results.artists.map((item) =>
            createResult(
              item,
              urlParams.get("q"),
              `artists/${item.id}`,
              "name",
              "label",
              [],
              ["bio"]
            )
          )}
        </TabPane>
        <TabPane tabId="albums">
        {results.albums.map((item) =>
            createResult(
              item,
              urlParams.get("q"),
              `albums/${item.id}`,
              "name",
              "artistName",
              [],
              ["description"]
            )
          )}
        </TabPane>
      </TabContent>
    </>
  );
};

const createResult = (
  data,
  query,
  link,
  titleAttribute,
  subtitleAttribute,
  shortSearchableAttributes,
  longSearchableAttributes
) => {
  let snippet = undefined;
  for (const attr of shortSearchableAttributes) {
    const value = data[attr];
    const possibleSnippet = highlight(query, value);
    if (possibleSnippet) {
      snippet = (
        <>
          <b>{`${camelToDisplay(attr)}: `}</b>
          {possibleSnippet}
        </>
      );
      break;
    }
  }
  if (!snippet) {
    for (const attr of longSearchableAttributes) {
      const value = data[attr];
      const rawSnippet = getSnippet(query, value);

      if (rawSnippet) {
        snippet = (
          <>
            <b>{`${camelToDisplay(attr)}: `}</b>
            ...{highlight(query, rawSnippet)}...
          </>
        );
        break;
      }
    }
  }

  return (
    <Card
      key={data.id}
      className="result-card text-start"
      color="dark"
      onClick={() => {
        window.location.href = link;
      }}
    >
      <CardBody>
        <CardTitle tag="h5">
          {highlight(query, data[titleAttribute]) ?? data[titleAttribute]}
        </CardTitle>
        <CardSubtitle className="mb-2 text-muted" tag="h6">
          {highlight(query, data[subtitleAttribute]) ?? data[subtitleAttribute]}
        </CardSubtitle>
        <CardText>{snippet}</CardText>
      </CardBody>
    </Card>
  );
};

export default SearchLanding;
