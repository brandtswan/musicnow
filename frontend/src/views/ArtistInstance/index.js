import React, { useState, useEffect } from "react";
import {
  Card,
  CardText,
  CardBody,
  CardTitle,
  Spinner,
  CardImg,
  Row,
  Col,
} from "reactstrap";

import "./index.css";

import NotFound from "../../components/NotFound";
import { getArtist } from "../../apis";
import Loading from "../../components/Loading";

const ArtistInstance = (props) => {
  const artistId = Number(props.match.params.id);

  const [artistObj, setArtistObj] = useState();

  useEffect(() => {
    async function fetchArtist() {
      const _artistObj = await getArtist(artistId);
      setArtistObj(_artistObj);
    }
    fetchArtist();
  }, [artistId]);

  if (artistObj === null) {
    return <NotFound></NotFound>;
  }

  if (!artistObj) {
    return <Loading></Loading>;
  }

  return (
    <div className="artist-instance-background">
      <Card style={{ width: "50%", minWidth: "350px", margin: "auto" }}>
        <div className="artist-image-container">
          <CardImg src={artistObj.image}></CardImg>
        </div>
        <CardBody>
          <CardTitle tag="h5">{artistObj.name}</CardTitle>
          {/* <CardTitle tag="h5">By <a href={`/artists/${albumObj.artistId}`}>{albumObj.artistName}</a></CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">{`Release date: ${albumObj.releaseDate ? new Date(albumObj.releaseDate).toLocaleDateString() : "N/A"}`}</CardSubtitle> */}
          <CardText
            className="artist-description-text"
            dangerouslySetInnerHTML={{ __html: artistObj.bio }}
          ></CardText>
        </CardBody>
      </Card>
      <Row>
        <h1 className="display-5 text-light">Details</h1>
        <Card>
          <Row>
            <Col>
              <h1 className="display-7 text-dark">Twitter</h1>
              <CardText>
                <a
                  href={`https://${artistObj.twitter}`}
                  target="_blank"
                  rel="noreferrer"
                >
                  {artistObj.twitter}
                </a>
              </CardText>
            </Col>
            <Col>
              <h1 className="display-7 text-dark">Label</h1>
              <CardText>{artistObj.label}</CardText>
            </Col>
            <Col>
              <h1 className="display-7 text-dark">Age</h1>
              <CardText>{artistObj.age}</CardText>
            </Col>
          </Row>
        </Card>
      </Row>
      <Row>
        <Col className="artist-songs-container">
          <h1 className="display-5 text-light">Similar Artists</h1>
          <div>
            {artistObj.similarArtists.map((artist) => {
              return (
                <Card className="artist-song-card">
                  <CardImg src={artist.image}></CardImg>
                  <CardTitle tag="h5">
                    <a href={`/artists/${artist.id}`}>{artist.name}</a>
                  </CardTitle>
                </Card>
              );
            })}
          </div>
        </Col>
        <Col className="artist-songs-container">
          <h1 className="display-5 text-light">Albums</h1>
          <div>
            {artistObj.albums.map((album) => {
              return (
                <Card className="artist-song-card">
                  <CardImg src={album.image}></CardImg>
                  <CardTitle tag="h5">
                    <a href={`/albums/${album.id}`}>{album.name}</a>
                  </CardTitle>
                </Card>
              );
            })}
          </div>
        </Col>
      </Row>
      <div className="artist-songs-container">
        <h1 className="display-5 text-light">Songs</h1>
        <div>
          {artistObj.songs.map((song) => {
            return (
              <Card className="artist-song-card">
                <CardTitle tag="h5">
                  <a href={`/songs/${song.id}`}>{song.name}</a>
                </CardTitle>
              </Card>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default ArtistInstance;
