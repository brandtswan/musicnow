import React, { useState, useEffect } from "react";
import {
  Card,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle
} from "reactstrap";


import "./index.css"

import NotFound from "../../components/NotFound";
import { getSong } from "../../apis";
import Loading from "../../components/Loading";

const SongInstance = (props) => {

  const songId = props.match.params.id;

  const [songObj, setSongObj] = useState();

  useEffect(() => {
    async function fetchAlbum() {
      const _albumObj = await getSong(songId);
      setSongObj(_albumObj);
    }
    fetchAlbum();
  }, [songId]);

  if (songObj === null) {
    return <NotFound></NotFound>;
  }

  if (!songObj) {
    return <Loading></Loading>;
  }

  const songMins = Math.floor(songObj.duration / 1000 / 60);
  const songSeconds = Math.round((songObj.duration / 1000) - songMins * 60);

  return (
    <div className="song-instance-background">
      <Card style={{ width: "50%", minWidth: "350px", margin: "auto" }}>
        <iframe title={`${songObj.name} | ${songObj.artistName}`}src={songObj.spotifyEmbed} width="100%" height="380" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>
        <CardBody>
          <CardTitle tag="h5">{songObj.name}</CardTitle>
          <CardTitle tag="h5">By <a href={`/artists/${songObj.artistId}`}>{songObj.artistName}</a></CardTitle>
          <CardTitle tag="h5">Album: <a href={`/albums/${songObj.albumId}`}>{songObj.albumName}</a></CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">{`Song duration: ${songMins > 0 ? `${songMins} minutes and ` : ""}${songMins > 0 ? `${songSeconds} seconds` : "N/A"}`}</CardSubtitle>
          <CardText className="song-description-text" dangerouslySetInnerHTML={{__html: songObj.description}}></CardText>
        </CardBody>
      </Card>
      <div>
      <div className="song-instance-lyrics-text">
        <h1 className="display-5 text-light">Lyrics</h1>
       {songObj.lyrics && songObj.lyrics.split("\n").map(line => <p>{line}</p>)}
       {!songObj.lyrics && <p>No lyrics found</p>}
      </div>
      </div>
    </div>
  );
};

export default SongInstance;
