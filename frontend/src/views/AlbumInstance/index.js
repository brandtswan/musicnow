import React, { useState, useEffect } from "react";
import {
  Card,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle
} from "reactstrap";


import "./index.css"

import NotFound from "../../components/NotFound";
import { getAlbum } from "../../apis";
import Loading from "../../components/Loading";

const AlbumInstance = (props) => {

  const albumId = Number(props.match.params.id);

  const [albumObj, setAlbumObj] = useState();

  useEffect(() => {
    async function fetchAlbum() {
      const _albumObj = await getAlbum(albumId);
      setAlbumObj(_albumObj);
    }
    fetchAlbum();
  }, [albumId]);

  if (albumObj === null) {
    return <NotFound></NotFound>;
  }

  if (!albumObj) {
    return <Loading></Loading>;
  }

  return (
    <div className="album-instance-background">
      <Card style={{ width: "70%", minWidth: "350px", margin: "auto"}}>
        <iframe title={`${albumObj.name} | ${albumObj.artistName}`}src={albumObj.spotifyEmbed} width="100%" height="380" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>
        <CardBody>
          <CardTitle tag="h5">{albumObj.name}</CardTitle>
          <CardTitle tag="h5">By <a href={`/artists/${albumObj.artistId}`}>{albumObj.artistName}</a></CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">{`Release date: ${albumObj.releaseDate ? new Date(albumObj.releaseDate).toLocaleDateString() : "N/A"}`}</CardSubtitle>
          <CardText className="album-description-text" dangerouslySetInnerHTML={{__html: albumObj.description}}></CardText>
        </CardBody>
      </Card>
      <div className="album-songs-container">
        <h1 className="display-5 text-light">Songs</h1>
        <div>
          {albumObj.songs.map(song => {
            return <Card className="album-song-card">
              <CardTitle tag="h5"><a href={`/songs/${song.id}`}>{song.name}</a></CardTitle>
            </Card>
          })}
        </div>
      </div>
    </div>
  );
};

export default AlbumInstance;
