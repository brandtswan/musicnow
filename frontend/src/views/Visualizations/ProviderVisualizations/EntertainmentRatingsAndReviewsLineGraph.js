import React, { useEffect, useState } from "react";
import { ResponsiveContainer, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from "recharts";

const PROVIDER_API_URL = "https://api.liveatx.me/api/entertainment?rating=";

const RATINGS = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5];

const EntertainmentRatingsAndReviewsLineGraph = (props) => {
    const [finalData, setFinalData] = useState([]);
    const [finishedLoading, setFinishedLoading] = useState(false);

    let allData = [];

    useEffect(() => {
        fetchAllData();
    }, []);

    const fetchAllData = async () => {
        for(let i = 0; i < RATINGS.length; i++) {
            const rating = RATINGS[i];
            await fetchSingleRatingData(rating);
        }
        setFinalData(allData);
        setFinishedLoading(true);
    }

    const fetchSingleRatingData = async (ratingValue) => {
        if(!finishedLoading) {
            try {
                const res = await fetch(PROVIDER_API_URL + ratingValue);
                const data = await res.json();
                let max = 0;
                let sum = 0;
                for(let i = 0; i < data["entertainment"].length; i++) {
                    const entertainment = data["entertainment"][i];
                    const reviewCount = parseInt(entertainment["review_count"]);
                    max = Math.max(max, reviewCount);
                    sum += reviewCount;
                }
                let avg = Math.round(sum / data["entertainment"].length);
                allData.push({"Rating": ratingValue, "Average Reviews": avg, "Max Reviews": max});
            } catch(err) {
                console.log("API ERROR", err);
            }
        }
    }

    if(!finishedLoading) {
        return <p style={{color: "white"}}>Loading visualization</p>
    }

    //followed simple line chart example from recharts website/documentation
    return (
        <ResponsiveContainer width="100%" height={600}>
            <LineChart width={500} height={600} data={finalData} margin={{top: 10, right: 50, left: 50, bottom: 50}}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="Rating" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Line type="monotone" dataKey="Average Reviews" stroke="#8884d8" activeDot={{r: 8}} />
                <Line type="monotone" dataKey="Max Reviews" stroke="#82ca9d" activeDot={{r: 8}} />
            </LineChart>
        </ResponsiveContainer>
    );
};

export default EntertainmentRatingsAndReviewsLineGraph;
