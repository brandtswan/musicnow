import React from "react";

import RestaurantRatingsBarChart from "./RestaurantRatingsBarChart";
import EntertainmentRatingsAndReviewsLineGraph from "./EntertainmentRatingsAndReviewsLineGraph";
import EventsEventTypePieChart from "./EventEventTypePieChart";

const ProviderVisualizations = (props) => {
    return (
        <div style={{color: "white", textAlign: "center"}}>
            <h1>Provider Visualizations</h1>
            <div>
                <h3>Number of restaurants per star rating</h3>
                <RestaurantRatingsBarChart />
                <h3>Max and average number of entertainment reviews per rating</h3>
                <EntertainmentRatingsAndReviewsLineGraph />
                <h3>Distributution of event types</h3>
                <EventsEventTypePieChart />
            </div>
        </div>
    );
};

export default ProviderVisualizations;