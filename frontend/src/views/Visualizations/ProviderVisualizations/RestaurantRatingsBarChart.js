import React, { useEffect, useState } from "react";
import { ResponsiveContainer, BarChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Bar, Label } from "recharts";

const PROVIDER_API_URL = "https://api.liveatx.me/api/restaurants?rating=";

const RATINGS = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5];

const RestaurantRatingsBarChart = (props) => {
    const [finalData, setFinalData] = useState([]);
    const [finishedLoading, setFinishedLoading] = useState(false);

    let allRatingsData = [];

    useEffect(() => {
        fetchAllRatingsData();
    }, []);

    const fetchAllRatingsData = async () => {
        for(let i = 0; i < RATINGS.length; i++) {
            const rating = RATINGS[i];
            await fetchSingleRatingData(rating);
        }
        setFinalData(allRatingsData);
        setFinishedLoading(true);
    }

    const fetchSingleRatingData = async (rating) => {
        if(!finishedLoading) {
            try {
                const res = await fetch(PROVIDER_API_URL + rating);
                const data = await res.json();
                allRatingsData.push({"Rating": rating, "Number of Restaurants": data["restaurants"].length});
            } catch(err) {
                console.log("API ERROR", err);
            }
        }
    }

    if(!finishedLoading) {
        return <p style={{color: "white"}}>Loading visualization.</p>
    }    

    //followed simple bar chart example from recharts website/documentation
    return (
        <ResponsiveContainer width="100%" height={600}>
            <BarChart width={500} height={600} data={finalData} margin={{top: 10, right: 50, left: 50, bottom: 50}}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="Rating"></XAxis>
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="Number of Restaurants" fill="#8884d8" />
            </BarChart>  
        </ResponsiveContainer>       
    );
};

export default RestaurantRatingsBarChart;
