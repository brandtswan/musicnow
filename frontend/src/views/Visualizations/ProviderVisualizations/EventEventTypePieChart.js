import React, { useEffect, useState } from "react";
import { PieChart, Pie, ResponsiveContainer } from "recharts";

const PROVIDER_API_URL = "https://api.liveatx.me/api/events";

const colors = [
  "#008ada",
  "#402cff",
  "#00c82c",
  "#ff9899",
  "#ffba00",
  "#ff2fd6",
  "#00e7ce",
  "#e92b00",
];

const EventsEventTypePieChart = (props) => {
  const [finalData, setFinalData] = useState([]);
  const [finishedLoading, setFinishedLoading] = useState(false);

  useEffect(() => {
    fetchAllData();
  }, []);

  const fetchAllData = async () => {
    const res = await fetch(PROVIDER_API_URL);
    const { events } = await res.json();

    const eventTypes = events.map((event) => event.event_type);
    const eventTypesCount = eventTypes.reduce((acc, curr) => {
      if (acc[curr]) {
        acc[curr]++;
      } else {
        acc[curr] = 1;
      }
      return acc;
    }, {});
    const eventTypesCountArray = Object.keys(eventTypesCount).map((key, i) => {
      return { name: key, value: eventTypesCount[key], fill: colors[i] };
    });

    setFinalData(eventTypesCountArray);
    setFinishedLoading(true);
  };

  if (!finishedLoading) {
    return <p style={{ color: "white" }}>Loading visualization</p>;
  }

  const renderLabel = function (entry) {
    const sum = finalData.reduce((acc, curr) => acc + curr.value, 0);
    return `${entry.name
      .split("_")
      .map((_) => _.charAt(0).toUpperCase() + _.slice(1))
      .join(" ")}: ${entry.value} (${((entry.value / sum) * 100).toFixed(2)}%)`;
  };

  //followed simple pie chart example from recharts website/documentation
  return (
    <ResponsiveContainer width="100%" height={600}>
      <PieChart width={800} height={800}>
        <Pie
          dataKey="value"
          isAnimationActive={true}
          data={finalData}
          cx="50%"
          cy="40%"
          innerRadius={150}
          outerRadius={200}
          label={renderLabel}
        />
      </PieChart>
    </ResponsiveContainer>
  );
};

export default EventsEventTypePieChart;
