import React, { useEffect, useState } from 'react';
import { Radar, RadarChart, PolarGrid, PolarAngleAxis, Legend, PolarRadiusAxis, ResponsiveContainer } from 'recharts';
import { getArtists } from "../../../apis";

const GENRES = [
  {genre: 'pop', a: 0, b: 0, c: 0},
  {genre: 'hip hop', a: 0, b: 0, c: 0},
  {genre: 'rock', a: 0, b: 0, c: 0},
  {genre: "pop rap", a: 0, b: 0, c: 0},
  {genre: "modern rock", a: 0, b: 0, c: 0},
];

const ArtistAgeRadarChart = () => {
  const [data, setData] = useState(undefined);

  useEffect(() => {
    fetchAllArtists();
  }, []);

  const fetchAllArtists = async () => {
    const allArtists = await (
      getArtists(
      999999,
      0,
      "age",
      undefined,
      undefined,
      undefined,
      undefined
    ))
    allArtists.page.forEach(artist => {
      const curGenres = []
      for(let i = 0; i < GENRES.length; i++) {
        if(artist.genres.includes(GENRES[i].genre)) {
          curGenres.push(i)
        }
      }

      if(!artist.age){
      } else
      if(artist.age >= 40) {
        curGenres.forEach(index => GENRES[index].a+=1)
      } else
      if(artist.age >= 30) {
        curGenres.forEach(index => GENRES[index].b+=1)
      } else {
        curGenres.forEach(index => GENRES[index].c+=1)
      }
      console.log(artist.genres)
    });
    //console.log(GENRES)

    setData(GENRES);
  };

  if (!data) {
    return <p style={{ color: "white" }}>Loading visualization.</p>;
  }

  const COLORS = [
    "#EE00FF",
    "#0088FE",
    "#00C49F",
    "#FFBB28",
    "#FF8042",
    "#FF3333",
    "#AA0000",
  ];

  return (
    <ResponsiveContainer width="100%" height={600}>
      <RadarChart cx="50%" cy="50%" outerRadius="80%" data={data}>
        <PolarGrid />
        <PolarAngleAxis dataKey="genre" stroke="#FFFFFF" />
        <PolarRadiusAxis angle={30} domain={[0, 40]} />
        <Radar name="<30" dataKey="c" stroke={COLORS[4]} fill={COLORS[4]} fillOpacity={0.6} />
        <Radar name="30-45" dataKey="b" stroke={COLORS[2]} fill={COLORS[2]} fillOpacity={0.6} />
        <Radar name=">45" dataKey="a" stroke={COLORS[0]} fill={COLORS[0]} fillOpacity={0.6} />
        <Legend />
      </RadarChart>
    </ResponsiveContainer>
  );
};

export default ArtistAgeRadarChart;
