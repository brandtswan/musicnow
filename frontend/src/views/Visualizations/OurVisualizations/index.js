import React from "react";

import SongDurationPieChart from "./SongDurationPieChart";
import AlbumSongCountBarChart from "./AlbumSongCountBarChart";
import ArtistAgeRadarChart from "./ArtistAgeRadarChart";

const OurVisualizations = (props) => {
    return (
        <div style={{color: "white", textAlign: "center"}}>
            <h1>Our Visualizations</h1>
            <div>
                <h3>Number of songs in each duration group</h3>
                <SongDurationPieChart />
                <h3>Song count of various albums</h3>
                <AlbumSongCountBarChart />
                <h3>Genre breakdown of various age groups</h3>
                <ArtistAgeRadarChart />
                {/*insert last of our visualizations here*/}
            </div>
        </div>
    );
};

export default OurVisualizations;
