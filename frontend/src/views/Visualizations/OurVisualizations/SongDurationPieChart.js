import React, { useEffect, useState } from "react";
import { ResponsiveContainer, PieChart, Pie, Cell, Legend } from "recharts";
import { getSongs } from "../../../apis";

const BUCKETS = [
  { label: " < 1:00", range: [0, 60] },
  { label: "1:00-1:59", range: [60, 120] },
  { label: "2:00-2:59", range: [120, 180] },
  { label: "3:00-3:59", range: [180, 240] },
  { label: "4:00-4:59", range: [240, 300] },
  { label: "5:00-5:59", range: [300, 360] },
  { label: " > 6:00", range: [360, 999999999999] },
];

const SongDurationPieChart = () => {
  const [data, setData] = useState(undefined);

  useEffect(() => {
    fetchAllDurations();
  }, []);

  const fetchAllDurations = async () => {
    const result = await Promise.all(
      BUCKETS.map((item) => {
        return fetchDuration(item.range);
      })
    );
    const finalData = result.map((item, index) => {
      return { name: BUCKETS[index].label, value: item };
    });

    setData(finalData);
  };

  const fetchDuration = async (range) => {
    const result = await getSongs(
      999999,
      0,
      undefined,
      undefined,
      undefined,
      range.map((x) => x * 1000)
    );
    return result.totalRecords;
  };

  const COLORS = [
    "#EE00FF",
    "#0088FE",
    "#00C49F",
    "#FFBB28",
    "#FF8042",
    "#FF3333",
    "#AA0000",
  ];

  // Percentage label creation from recharts examples
  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
    index,
  }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text
        x={x}
        y={y}
        fill="white"
        textAnchor={x > cx ? "start" : "end"}
        dominantBaseline="central"
      >
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };

  if (!data) {
    return <p style={{ color: "white" }}>Loading visualization.</p>;
  }

  return (
    <ResponsiveContainer width="100%" height={600}>
      <PieChart width={600} height={600} margin={{top: 10, right: 50, left: 50, bottom: 50}}>
        <Legend/>
        <Pie
          data={data}
          cx="50%"
          cy="50%"
          labelLine={true}
          label={true}
          outerRadius={200}
          fill="#8884d8"
          dataKey="value"
        >
          {data.map((_, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
        </Pie>
      </PieChart>
    </ResponsiveContainer>
  );
};

export default SongDurationPieChart;
