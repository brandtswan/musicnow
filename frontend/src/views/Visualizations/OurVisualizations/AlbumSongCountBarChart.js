import React, { useEffect, useState } from "react";
import { ResponsiveContainer, BarChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Bar, Label } from "recharts";

const OUR_API_URL = "https://api.music-now.me/albums?filterSongCount=";

const SONG_COUNTS = [[0, 5], [6, 10], [11, 15], [16, 20], [21, 25], [26, 30], [31, 35], [36, 40], [41, 45], [46, 50]];

const AlbumSongCountBarChart = (props) => {
    const [finalData, setFinalData] = useState([]);
    const [finishedLoading, setFinishedLoading] = useState(false);

    let allData = [];

    useEffect(() => {
        fetchAllSongCountData();
    }, []);

    const fetchAllSongCountData = async () => {
        for(let i = 0; i < SONG_COUNTS.length; i++) {
            const range = SONG_COUNTS[i];
            await fetchSingleRangeData(range);
        }
        setFinalData(allData);
        setFinishedLoading(true);
    }

    const fetchSingleRangeData = async (range) => {
        if(!finishedLoading) {
            try {
                const res = await fetch(OUR_API_URL + range[0] + "," + range[1]);
                const data = await res.json();
                allData.push({"Song Count": range[0] + "-" + range[1], "Number of Albums": data["totalRecords"]});
            } catch(err) {
                console.log("API ERROR", err);
            }
        }
    }

    if(!finishedLoading) {
        return <p style={{color: "white"}}>Loading visualization.</p>
    }    

    return (
        <ResponsiveContainer width="100%" height={600}>
            <BarChart width={700} height={600} data={finalData} margin={{top: 10, right: 50, left: 50, bottom: 50}}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="Song Count"><Label value="Song Counts" position="bottom"></Label></XAxis>
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="Number of Albums" fill="#8884d8" />
            </BarChart>
        </ResponsiveContainer>
        
    );
};

export default AlbumSongCountBarChart;
