import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Card, CardTitle, CardSubtitle, CardText, CardImg, CardBody, CardLink } from 'reactstrap';

import Member from './member';
//background image doesn't work too well with scrolling currently, so a solid color is opted for for now until a better solution is found
//import AboutBackground from '../../images/AboutBackground.jpg';
import GitLabPic from '../../images/GitLabLogo.png';
import PostmanPic from '../../images/PostmanLogo.png';

import BrandtPic from './team pictures/Brandt.jpg';
import DarshanPic from './team pictures/Darshan.jpg';
import DavidPic from './team pictures/David.png';
import MasonPic from './team pictures/Mason.jpg';
import MichaelPic from './team pictures/Michael.jpg';

const POSTMAN_URL = 'https://documenter.getpostman.com/view/17710004/UUy39mLR';
const GITLAB_URL = 'https://gitlab.com/brandtswan/musicnow';
//per_page is set to a very high number just to make sure all issues/commits are always grabbed (only commits on main branch are grabbed)
const GITLAB_COMMITS_URL = 'https://gitlab.com/api/v4/projects/29951527/repository/commits?per_page=500';
const GITLAB_ISSUES_URL = 'https://gitlab.com/api/v4/projects/29951527/issues?per_page=500';

const TOOLS = require('./toolsinfo.json');
const APIS = require('./apisinfo.json');

const TOTAL_UNIT_TESTS = 66;

const About = (props) => {
    //fetches and sets the relevant gitlab api data
    //runs every render
    useEffect(() => {
        fetchCommits();
        fetchIssues();
    }, []);

    const [totalCommits, setTotalCommits] = useState(0);
    const [totalIssues, setTotalIssues] = useState(0);

    //gitlab repo stats scraping based on AroundATX https://gitlab.com/jyotiluu/cs373-aroundatx/-/blob/master/frontend/src/Pages/About/About.js
    //gets all the commit data to determine overall number and commits per person
    const fetchCommits = async () => {
        const response = await fetch(GITLAB_COMMITS_URL);
        const commits = await response.json();
        setTotalCommits(commits.length);
    };

    //gets all the issues data to determine overall number and commits per person
    const fetchIssues = async () => {
        const response = await fetch(GITLAB_ISSUES_URL);
        const issues = await response.json();
        setTotalIssues(issues.length);
    };

    return (
        <div style={{backgroundColor: "dimgray"}}>
            <Container style={{paddingTop: "5%"}}>
                <Row>
                    <Col id="about-card" xs="12">
                        <Card style={{marginBottom: "5%", padding: "5%", borderRadius: "50px"}}>
                            <CardBody>
                                <CardTitle tag="h1" style={{marginBottom: "1%", fontWeight: "bold"}}>MusicNow</CardTitle>
                                <CardSubtitle tag="h3">MusicNow is a site for those who want to learn more about their favorite songs, artists, and albums -- and for those who want to learn more about great music in general!
                                    See lots of information about many songs, artists, and albums, and learn how they all connect to each other in the massive web that is modern music.
                                    By using our site, music enthusiasts can discover fun facts, such as how artists frequently feature other artists of the same genre in their collabs, but less so other genres' artists.
                                </CardSubtitle>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Member name="Brandt Swanson" pic={BrandtPic}/>

                    <Col id="about-card" xs="12" md="4" style={{marginBottom: "5%"}}>
                        <Card style={{height: "100%", padding: "5%", borderRadius: "50px"}}>
                            <CardBody>
                                <CardText>
                                    <Container>
                                        <CardImg top style={{width: "100%", height: "auto"}} src={GitLabPic} />
                                        <CardText style={{fontSize: "2em"}}><CardLink style={{color: "black"}} href={GITLAB_URL}>GitLab Repository Info</CardLink></CardText>
                                        <Row style={{padding: "2%", marginBottom: "2%"}}>
                                            <Col>
                                                {totalCommits} <br></br> commits
                                            </Col>
                                            <Col>
                                                {totalIssues} <br></br> issues
                                            </Col>
                                            <Col>
                                                {TOTAL_UNIT_TESTS} <br></br> unit tests
                                            </Col>
                                        </Row>
                                    </Container>
                                    <Container>
                                        <CardImg top style={{width: "100%", height: "auto"}} src={PostmanPic} />
                                        <CardText style={{fontSize: "2em"}}><CardLink style={{color: "black"}} href={POSTMAN_URL}>Postman API Documentation</CardLink></CardText>
                                    </Container>
                                </CardText>
                            </CardBody>
                        </Card>
                    </Col>

                    <Member name="Darshan Bhatta" pic={DarshanPic}/>
                    
                </Row>
                <Row>
                    <Member name="David Qi" pic={DavidPic}/>
                    <Member name="Mason Eastman" pic={MasonPic}/>
                    <Member name="Michael Cao" pic={MichaelPic}/>
                </Row>

                <Row>
                    <Col id="about-card" xs="12" md="6" style={{marginBottom: "5%"}}>
                        <Card style={{height: "100%", padding: "5%", borderRadius: "50px"}}>
                            <CardTitle style={{fontSize: "2em"}}>Tools Used</CardTitle>
                            <CardBody>
                                {TOOLS.map(tool => {
                                    return <CardText style={{textAlign: "left"}}><CardLink style={{fontStyle: "italic"}} href={tool.link}>{tool.name}</CardLink>: {tool.description}</CardText>
                                })}
                            </CardBody>
                        </Card>
                    </Col>

                    <Col id="about-card" xs="12" md="6" style={{marginBottom: "5%"}}>
                        <Card style={{height: "100%", padding: "5%", borderRadius: "50px"}}>
                            <CardTitle style={{fontSize: "2em"}}>APIs Used</CardTitle>
                            <CardBody>
                                {APIS.map(api => {
                                    return <CardText style={{textAlign: "left"}}><CardLink style={{fontStyle: "italic"}} href={api.link}>{api.name}</CardLink>: {api.description}</CardText>
                                })}
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <br></br>
            </Container>
        </div>
    );
};

export default About;
