import React, { useEffect, useState } from "react";
import { Row, Card, Col, Container, CardTitle, CardSubtitle, CardText, CardImg, CardBody, CardLink } from 'reactstrap';

//formatting of team info data based on AroundATX https://gitlab.com/jyotiluu/cs373-aroundatx/-/blob/master/frontend/src/Pages/About/member-info.json
const TEAM_INFO = require('./teaminfo.json');

const GITLAB_MEMBER_URL = 'https://gitlab.com/'
//per_page is set to a very high number just to make sure all issues/commits are always grabbed (only commits on main branch are grabbed)
const GITLAB_COMMITS_URL = 'https://gitlab.com/api/v4/projects/29951527/repository/commits?per_page=500';
const GITLAB_ISSUES_URL = 'https://gitlab.com/api/v4/projects/29951527/issues?per_page=500&all=true&assignee_username=';

const Member = (props) => {
    //fetches and sets the relevant gitlab api data
    //runs every render
    useEffect(() => {
        fetchMemberCommits();
        fetchMemberIssues();
    });

    const [memberCommits, setMemberCommits] = useState(0);
    const [memberIssues, setMemberIssues] = useState(0);

    //gitlab member stats scraping based on AroundATX https://gitlab.com/jyotiluu/cs373-aroundatx/-/blob/master/frontend/src/Pages/About/InfoCard.js

    //gets the number of commits from this team member
    const fetchMemberCommits = async () => {
        let response = await fetch(GITLAB_COMMITS_URL);
        let commits = await response.json();
        let tally = 0;
        commits.forEach(commit => {
            if(TEAM_INFO[props.name]["email"].includes(commit["author_email"])) {
                tally++;
            }
        });
        setMemberCommits(tally);
    }

    //gets the number of issues for this teammember
    const fetchMemberIssues = async () => {
        let response = await fetch(GITLAB_ISSUES_URL + TEAM_INFO[props.name]["gitlab_id"]);
        let issues = await response.json();
        setMemberIssues(issues.length);
    }

    //returns one member card, that the index.js file will combine into a grid of information
    return (
        <Col id="about-card" xs="12" md="4" style={{marginBottom: "5%"}}>
            <Card style={{height: "100%", padding: "5%", borderRadius: "50px"}}>
                <CardImg style={{borderRadius: "50px"}} top src={props.pic} alt={props.name + ' picture'}/>
                <CardBody>
                    <CardTitle style={{fontSize: "2em"}} ><CardLink style={{color: "black"}} href={GITLAB_MEMBER_URL + TEAM_INFO[props.name]["gitlab_id"]}>{props.name}</CardLink></CardTitle>
                    <CardSubtitle>{TEAM_INFO[props.name]["responsibility"]}</CardSubtitle>
                    <CardText style={{fontSize: "1.2em"}}>{TEAM_INFO[props.name]["bio"]}</CardText>
                    <CardText>
                        <Container>
                            <Row style={{padding: "2%"}}>
                                <Col>
                                    {memberCommits} <br></br> commits
                                </Col>
                                <Col>
                                    {memberIssues} <br></br> issues
                                </Col>
                                <Col>
                                    {TEAM_INFO[props.name]["unit_tests"]} <br></br> unit tests
                                </Col>
                            </Row>
                        </Container>
                    </CardText>
                </CardBody>
            </Card>
        </Col>        
    );
};

export default Member;
