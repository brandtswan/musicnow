import { BrowserRouter, Route, Switch } from "react-router-dom";

import "./App.css";

import Home from "./views/Home";
import About from "./views/About";
import SongsList from "./views/lists/SongsList";
import ArtistsList from "./views/lists/ArtistsList";
import AlbumsList from "./views/lists/AlbumsList";
import SongInstance from "./views/SongInstance";
import AlbumInstance from "./views/AlbumInstance";
import ArtistInstance from "./views/ArtistInstance";
import SearchLanding from "./views/SearchLanding";
import OurVisualizations from "./views/Visualizations/OurVisualizations";
import ProviderVisualizations from "./views/Visualizations/ProviderVisualizations";

import NavigationBar from "./components/NavigationBar";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <div className="app-container">
          <NavigationBar></NavigationBar>
          <div className="page-container">
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/about" exact component={About} />
              <Route path="/songs" exact component={SongsList} />
              <Route path="/artists" exact component={ArtistsList} />
              <Route path="/albums" exact component={AlbumsList} />
              <Route path="/songs/:id" exact component={SongInstance} />
              <Route path="/albums/:id" exact component={AlbumInstance} />
              <Route path="/artists/:id" exact component={ArtistInstance} />
              <Route path="/search" exact component={SearchLanding} />
              <Route path="/ourvisualizations" exact component={OurVisualizations} />
              <Route path="/providervisualizations" exact component={ProviderVisualizations} />
            </Switch>
          </div>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
