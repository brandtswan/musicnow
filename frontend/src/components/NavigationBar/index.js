import React, { useState } from "react";
import {
  Button,
  Input,
  InputGroup,
  Navbar,
  Nav,
  NavbarBrand,
  NavLink,
  NavItem,
} from "reactstrap";
import { MdSearch } from "react-icons/md";

import "./index.css";

const NavigationBar = (props) => {
  const [tempQuery, setTempQuery] = useState("");
  
  const makeSearch = (query) => {
    window.location.href = `/search?q=${query}`;
  };

  return (
    <Navbar className="navbar-main">
      <NavbarBrand className="navbar-logo" href="/">
        <h4>MusicNow</h4>
      </NavbarBrand>
      <Nav>
        <NavLink href="/songs" className="navbar-link">
          Songs
        </NavLink>
        <NavLink href="/artists" className="navbar-link">
          Artists
        </NavLink>
        <NavLink href="/albums" className="navbar-link">
          Albums
        </NavLink>
        <NavLink href="/ourvisualizations" className="navbar-link">
          Our Visualizations
        </NavLink>
        <NavLink href="/providervisualizations" className="navbar-link">
          Provider Visualizations
        </NavLink>
        <NavLink href="/about" className="navbar-link final-navbar-link">
          About Us
        </NavLink>
        <NavItem className="navbar-search">
          <InputGroup>
            <Input
              id="search"
              name="search"
              type="search"
              placeholder="Search"
              onChange={(e) => {
                let query = e.target.value.trim();
                setTempQuery(query);
              }}
              onKeyDown={(e) => {
                let query = e.target.value.trim();
                if (e.key === "Enter" && query !== "") {
                  makeSearch(query);
                }
              }}
            />
            <Button
              onClick={() => {
                makeSearch(tempQuery);
              }}
            >
              <MdSearch />
            </Button>
          </InputGroup>
        </NavItem>
      </Nav>
    </Navbar>
  );
};

export default NavigationBar;
