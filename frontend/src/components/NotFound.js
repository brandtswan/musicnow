import React from "react";
import {
    Alert,
  } from "reactstrap";
import { BiErrorCircle } from 'react-icons/bi';

const NotFound = () => {
    return (
        <div className="song-instance-background">
          <Alert color="warning">
            {"Uh oh! This page does not exist. Please go back to the home page."}
          </Alert>
          <BiErrorCircle size="10em" color="white"></BiErrorCircle>
        </div>
      );
  };
  
  export default NotFound;
  