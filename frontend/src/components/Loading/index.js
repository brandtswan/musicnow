import React from "react";
import {
    Alert,
    Spinner
  } from "reactstrap";

import "./index.css";

const Loading = () => {
    return (
        <div className="loading-background">
          <Alert color="light">
            {"Loading..."}
          </Alert>
          <Spinner color="light" children=""/>
        </div>
      );
  };
  
  export default Loading;
  