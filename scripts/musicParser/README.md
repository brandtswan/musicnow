# Music Parser
This script folder is able to scrape artist, album and song data from the following APIs.

1. LastFM
2. Spotify
3. Genius

## How it works
The script starts out with finding the top artists and one level down of the similar artists. Then finds the top albums of those artists and for those albums finds the songs.

## How to run
Before running set up the following env variables.
```
LAST_FM_API_KEY=xxx

export SPOTIFY_ACCESS_TOKEN=xxx
export SPOTIFY_REFRESH_TOKEN=xxx

export GENIUS_API_KEY=xxx
```

To generate the spotify tokens, please use [this site](https://getyourspotifyrefreshtoken.herokuapp.com/) and input the client id and secret of your dev account.

Run the parse.js file in the src folder. To edit any configuration like how many artists to fetch, albums, etc. Look at the respective files in the `lib` folder.