const axios = require("axios");

async function makeRequest(config) {
    return axios(config)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
}

async function getArtistInfo(artistName) {
    var config = {
      method: "get",
      url: `https://www.theaudiodb.com/api/v1/json/1/search.php?s=${encodeURIComponent(artistName)}`,
      headers: {},
    };
    return makeRequest(config);
  }

module.exports = {
    getArtistInfo
}
