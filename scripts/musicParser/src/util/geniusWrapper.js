const geniusAPI = require("genius-lyrics-api");

async function getLyrics(songName, artistName) {
    return geniusAPI.getLyrics({
        apiKey: process.env.GENIUS_API_KEY,
        title: songName,
        artist: artistName,
        optimizeQuery: true
    })
}

module.exports = {
    getLyrics
}