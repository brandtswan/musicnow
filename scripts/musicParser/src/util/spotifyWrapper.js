var SpotifyWebApi = require("spotify-web-api-node");

// credentials are optional
const spotifyApi = new SpotifyWebApi({
  accessToken:
    process.env.SPOTIFY_ACCESS_TOKEN,
  refreshToken:
    process.env.SPOTIFY_REFRESH_TOKEN,
});

async function getSpotifySongId(songName, artistName) {
  const data = await spotifyApi.searchTracks(`track:${songName} artist:${artistName}`);
  return data.body.tracks.items[0].id;
}

async function getArtistInfo(artistName) {
  const data = await spotifyApi.searchArtists(artistName);
  return data.body.artists.items[0];
}



async function getAlbumInfo(albumName, artistName) {
  const data = await spotifyApi.searchAlbums(`album:${albumName} artist:${artistName}`);
  return data.body.albums.items[0];
}

async function getAllSongsInAlbumById(spotifyAlbumId) {
  const data = await spotifyApi.getAlbumTracks(spotifyAlbumId, {
    limit: 50
  });
  return data.body.items;
}

module.exports = {
  getSpotifySongId,
  getArtistInfo,
  getAlbumInfo,
  getAllSongsInAlbumById
};
