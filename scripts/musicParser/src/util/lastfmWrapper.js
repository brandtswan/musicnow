const axios = require("axios");

async function makeRequest(config) {
    return axios(config)
      .then(function (response) {
        return response.data;
      })
      .catch(function (error) {
        console.log(error);
      });
}

async function getTopCharts() {
  var config = {
    method: "get",
    url: `https://ws.audioscrobbler.com/2.0/?method=chart.gettoptracks&api_key=${process.env.LAST_FM_API_KEY}&format=json`,
    headers: {},
  };
  return makeRequest(config);
}

async function getSongInfo(songName, artistName) {
    var config = {
      method: "get",
      url: `https://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=${process.env.LAST_FM_API_KEY}&artist=${encodeURIComponent(artistName)}&track=${encodeURIComponent(songName)}&format=json`,
      headers: {},
    };
    return makeRequest(config);
}

async function getTopArtists(page = 1) {
  var config = {
    method: "get",
    url: `https://ws.audioscrobbler.com/2.0/?api_key=${process.env.LAST_FM_API_KEY}&format=json&method=chart.gettopartists&page=${page}`,
    headers: {},
  };
  return makeRequest(config);
}

async function getArtistInfo(artistName) {
  var config = {
    method: "get",
    url: `http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=${encodeURIComponent(artistName)}&api_key=${process.env.LAST_FM_API_KEY}&format=json`,
    headers: {},
  };
  return makeRequest(config);
}

async function getArtistTopAlbums(artistName) {
  var config = {
    method: "get",
    url: `http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=${encodeURIComponent(artistName)}&api_key=${process.env.LAST_FM_API_KEY}&format=json`,
    headers: {},
  };
  return makeRequest(config);
}

async function getAlbumInfo(albumName, artistName) {
  var config = {
    method: "get",
    url: `http://ws.audioscrobbler.com/2.0/?method=album.getinfo&api_key=${process.env.LAST_FM_API_KEY}&artist=${encodeURIComponent(artistName)}&album=${encodeURIComponent(albumName)}&format=json`,
    headers: {},
  };
  return makeRequest(config);
}

module.exports = {
    getTopCharts,
    getSongInfo,
    getTopArtists,
    getArtistInfo,
    getArtistTopAlbums,
    getAlbumInfo
}
