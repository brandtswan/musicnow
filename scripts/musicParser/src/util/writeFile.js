const fs = require("fs");

async function writeFile(fileName, data) {
    return new Promise((res, rej) => {
      fs.writeFile(
        `${__dirname}/${fileName}.json`,
        JSON.stringify(data, null, 2),
        function (err) {
          if (err) rej();
          res();
        }
      );
    })
}

module.exports = writeFile;