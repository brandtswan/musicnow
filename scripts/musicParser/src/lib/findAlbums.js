const lastFmAPI = require("../util/lastfmWrapper");
const spotifyApi = require("../util/spotifyWrapper");

let idCount = 0;

async function findAlbums(artists) {
    const parsedAlbums = [];

    const len = artists.length;
    for (let i = 0; i < len; i++) {
        console.log(`Albums | ${i}/${len}`)

        const artistName = artists[i].name;

        const lastFmAlbums = await lastFmAPI.getArtistTopAlbums(artistName);
        
        for (let j = 0; j < Math.min(3, lastFmAlbums.topalbums.album.length); j++) {
            try {
                const lastFmAlbum = lastFmAlbums.topalbums.album[j];
            
                const { album: lastFmAlbumInfo } = await lastFmAPI.getAlbumInfo(lastFmAlbum.name, artistName);
                const spotifyAlbumInfo = await spotifyApi.getAlbumInfo(lastFmAlbum.name, artistName);
    
                if (!spotifyAlbumInfo) continue;
    
                parsedAlbums.push({
                    id: idCount++,
                    name: lastFmAlbum.name,
                    releaseDate: spotifyAlbumInfo.release_date,
                    spotifyEmbed: `https://open.spotify.com/embed/album/${spotifyAlbumInfo.id}`,
                    image: spotifyAlbumInfo.images[0]?.url,
                    lastFmListeners: Number(lastFmAlbumInfo.listeners),
                    description: lastFmAlbumInfo.wiki?.content || `${lastFmAlbum.name} is an album ${lastFmAlbumInfo.tracks?.track?.length ? `with ${lastFmAlbumInfo.tracks.track.length} songs that was ` : ""}created by ${artists[i].name} with ${Number(lastFmAlbumInfo.listeners).toLocaleString()} plays in LastFM.`,
                    genres: Array.isArray(lastFmAlbumInfo.tags?.tag) ? lastFmAlbumInfo.tags?.tag?.map(tag => tag.name) : [],
                    artistId: artists[i].id,
                    artistName: artists[i].name,
                });
            } catch (err) {
                console.log("skipping ...", err);
            }
        }
    }
    return parsedAlbums;
}

module.exports = findAlbums;