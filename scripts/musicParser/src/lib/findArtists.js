const lastFmAPI = require("../util/lastfmWrapper");
const spotifyApi = require("../util/spotifyWrapper");


const nameToIdMap = new Map();
let idCount = 0;

async function findArtists() {
    const lastFmArtists = await lastFmAPI.getTopArtists();

    const parsedArtists = lastFmArtists.artists.artist.slice(0, 100).map(artist => {
        const id = idCount++;
        nameToIdMap.set(artist.name, id);
        return {
            id,
            name: artist.name,
            similarArtists: new Set()
        }
    });

    await parseArtists(parsedArtists, false);
    await parseArtists(parsedArtists, true);

    for (const parsedArtist of parsedArtists) {
        parsedArtist.similarArtists = [...parsedArtist.similarArtists]
    }
   
    return parsedArtists;
}

async function parseArtists(parsedArtists, ignoreSimilar) {
    const len = parsedArtists.length;
    for (let i = 0; i < len; i++) {
        console.log(`Artists | ${i}/${len}`)
        const artist = parsedArtists[i];

        if (artist.lastFmListeners) continue;

        const {artist: artistInfo} = await lastFmAPI.getArtistInfo(artist.name);

        const spotifyArtistInfo = await spotifyApi.getArtistInfo(artist.name);

        if (!ignoreSimilar) {
            artistInfo.similar.artist.filter(similarArtist => !similarArtist.name.includes("&")).forEach(similarArtist => {          
                let id = nameToIdMap.get(similarArtist.name);

                if (id === undefined) {
                    id = idCount++;
                    nameToIdMap.set(similarArtist.name, id)
                    parsedArtists.push({
                        id,
                        name: similarArtist.name,
                        similarArtists: new Set([artist.id])
                    });
                } else {
                    parsedArtists[id].similarArtists.add(artist.id)
                }
                parsedArtists[i].similarArtists.add(id)
            });
        }
 

        parsedArtists[i] = {
            ...artist,
            bio: artistInfo.bio.summary,
            genres: spotifyArtistInfo.genres,
            lastFmListeners: Number(artistInfo.stats.listeners),
            spotifyFollowers: spotifyArtistInfo.followers.total,
            image: spotifyArtistInfo.images[0]?.url
        }
        
    }
}

module.exports = findArtists;