const lastFmAPI = require("../util/lastfmWrapper");
const spotifyApi = require("../util/spotifyWrapper");
const geniusApi = require("../util/geniusWrapper");

let idCount = 0;

async function findSongs(albums) {
    const parsedSongs = [];

    const len = albums.length;
    for (let i = 0; i < len; i++) {
        console.log(`Songs | ${i}/${len}`);
        
        const { name: albumName, id: albumId, artistName, artistId, spotifyEmbed, releaseDate } = albums[i];

        const spotifyId = spotifyEmbed.split("/")[spotifyEmbed.split("/").length-1];

        const spotifySongs = await spotifyApi.getAllSongsInAlbumById(spotifyId);

        spotifySongs.forEach(async spotifySong => {
            // const lyrics = await geniusApi.getLyrics(spotifySong.name, artistName);

            const {track: lastFmSongInfo} = await lastFmAPI.getSongInfo(spotifySong.name, artistName);
            // console.log(lastFmSongInfo);

            parsedSongs.push({
                id: idCount++,
                name: spotifySong.name,
                artistName,
                artistId,
                lyrics: "",
                image: lastFmSongInfo.album?.image[lastFmSongInfo.album.image.length - 1]["#text"] || "https://lastfm.freetls.fastly.net/i/u/300x300/2a96cbd8b46e442fc41c2b86b821562f.png",
                albumName,
                albumId,
                duration: spotifySong.duration_ms,
                description: lastFmSongInfo.wiki?.summary || `${spotifySong.name} is a song created by ${artistName} with ${Number(lastFmSongInfo.playcount).toLocaleString()} plays in LastFM.`,
                genres: lastFmSongInfo.toptags.tag.map(tag => tag.name),
                spotifyEmbed: `https://open.spotify.com/embed/track/${spotifySong.id}`,
                releaseDate
            });
        })
        await new Promise(resolve => setTimeout(resolve, 1500));
    }
    return parsedSongs;
}

module.exports = findSongs;