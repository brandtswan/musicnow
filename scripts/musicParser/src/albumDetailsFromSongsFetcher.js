const writeFile = require("./util/writeFile");

const albums = require("./util/albums.json");

const songs = require("./util/songs.json");

start();

async function start() {
    let i = 0;

    for (const album of albums) {
        console.log(`${i++}/${albums.length}`);

        const _songs = songs.filter(song => song.albumId === album.id);
        

        album.songCount = _songs.length;
        album.totalDuration = _songs.reduce((sum, song) => sum + song.duration, 0);

        // writing file to save progress
        await writeFile("albums", albums);
    }
}
