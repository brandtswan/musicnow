const writeFile = require("./util/writeFile");

const findArtists = require("./lib/findArtists");
const findAlbums = require("./lib/findAlbums");
const findSongs = require("./lib/findSongs");


const SKIP_ARTIST = true;
const SKIP_ALBUM = true;

let artists = require("./artists.json");
let albums = require("./albums.json");

start();

async function start() {
  if (!SKIP_ARTIST) artists = await findArtists();
  await writeFile("artists", artists);

  if (!SKIP_ALBUM) albums = await findAlbums(artists);
  await writeFile("albums", albums);

  const songs = await findSongs(albums);
  await writeFile("songs", songs);
}
