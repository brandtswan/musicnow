const writeFile = require("./util/writeFile");

const artists = require("./util/artists.json");
const audiodbAPI = require("./util/audiodbWrapper");

start();

async function start() {
    let i = 0;

    for (const artist of artists) {
        console.log(`${i++}/${artists.length}`);

        if (artist.label) {
            console.log("already have skipping...")
            continue;
        }

        const rawArtistInfo = await audiodbAPI.getArtistInfo(artist.name);
        const artistInfo = rawArtistInfo.artists ? rawArtistInfo.artists[0] : null;

        artist.twitter = artistInfo?.strTwitter ?? null;
        artist.age = artistInfo?.intBornYear ? 2021 - Number(artistInfo.intBornYear) : 0;
        artist.label = artistInfo?.strLabel ?? "Unknown";

        // writing file to save progress
        await writeFile("artists", artists);
    }
}
