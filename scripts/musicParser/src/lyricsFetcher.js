const writeFile = require("./util/writeFile");

/**
 * The genius API has rate limits so we need to post process it.
 */

const songs = require("./util/songs.json");
const geniusApi = require("./util/geniusWrapper");

start();

async function start() {
    let i = 0;

    for (const song of songs) {
        console.log(`${i++}/${songs.length}`);

        if (song.lyrics) {
            console.log("already have skipping...")
            continue;
        }
        
        song.lyrics = await geniusApi.getLyrics(song.name, song.artistName);

        // writing file to save progress
        await writeFile("songs", songs);

        await new Promise(resolve => setTimeout(resolve, 1500));
    }
}
