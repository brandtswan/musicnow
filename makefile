.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

# get git status
status:
	@echo
	git branch
	git remote -v
	git status

#pull from git
pull:
	@echo
	git pull
	git status

# setup backend
setup-backend:
	cd backend && pip3 install -r requirements.txt

# start backend locally
start-backend:
	cd backend && python3 app.py &

# format backend python code
format-backend:
	cd backend && black *.py

# run backend unit tests on a specified hostname
# to run on production api, use 'https://api.music-now.me'
unit-tests:
	cd backend && API_HOSTNAME=$(API_HOSTNAME) python3 unit_tests.py

# setup frontend
setup-frontend:
	cd frontend && npm install 

#start the frontend locally
start-frontend:
	cd frontend && npm start &

# build docker image
build-docker:
	docker build -t musicnow .

# docker enviroment
docker:
	docker run -p 127.0.0.1:3000:3000/tcp -p 127.0.0.1:5000:5000/tcp --rm -i -t -v $(PWD):/app -w /app musicnow:latest

# start both frontend and backend
start:
	make start-frontend & make start-backend &

# stop both frontend and backend
stop:
	pkill python3 & pkill node &