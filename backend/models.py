from flask import Flask
from initdb import init_db
from flask_restful import Api, Resource, abort
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields

app = Flask(__name__)
api = Api(app)
db = init_db(app)

ma = Marshmallow(app)

# format based off TexasVotes https://gitlab.com/forbesye/fitsbits/-/blob/master/back-end/models.py


class Song(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String, nullable=False)
    artistName = db.Column(db.String, nullable=False)
    artistId = db.Column(db.Integer, primary_key=True, nullable=False)
    lyrics = db.Column(db.String, nullable=True)
    image = db.Column(db.String, nullable=True)
    albumName = db.Column(db.String, nullable=False)
    albumId = db.Column(db.Integer, primary_key=True, nullable=False)
    duration = db.Column(db.Integer, nullable=True)
    description = db.Column(db.String, nullable=True)
    genres = db.Column(db.ARRAY(db.String), nullable=True)
    spotifyEmbed = db.Column(db.String, nullable=True)
    releaseDate = db.Column(db.String, nullable=True)

    def __repr__(self):
        return "<Song %s %s>" % (self.name, self.id)


class Artist(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String, nullable=False)
    similarArtists = db.Column(db.ARRAY(db.Integer), nullable=True)
    bio = db.Column(db.String, nullable=True)
    genres = db.Column(db.ARRAY(db.String), nullable=True)
    lastFmListeners = db.Column(db.Integer, nullable=True)
    spotifyFollowers = db.Column(db.Integer, nullable=True)
    image = db.Column(db.String, nullable=True)
    twitter = db.Column(db.String, nullable=True)
    age = db.Column(db.Integer, nullable=True)
    label = db.Column(db.String, nullable=True)

    def __repr__(self):
        return "<Artist %s %s>" % (self.name, self.id)


class Album(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String, nullable=False)
    releaseDate = db.Column(db.String, nullable=True)
    spotifyEmbed = db.Column(db.String, nullable=True)
    lastFmListeners = db.Column(db.Integer, nullable=True)
    description = db.Column(db.String, nullable=True)
    genres = db.Column(db.ARRAY(db.String), nullable=True)
    artistId = db.Column(db.Integer, primary_key=True, nullable=False)
    artistName = db.Column(db.String, nullable=False)
    songCount = db.Column(db.Integer, nullable=False)
    totalDuration = db.Column(db.Integer, nullable=True)
    image = db.Column(db.String, nullable=True)

    def __repr__(self):
        return "<Album %s %s>" % (self.name, self.id)


class SongSchema(ma.Schema):
    id = fields.Int(required=True)
    name = fields.Str(required=True)
    artistName = fields.Str(required=True)
    artistId = fields.Int(required=True)
    lyrics = fields.Str(required=False)
    image = fields.Str(required=False)
    albumName = fields.Str(required=True)
    albumId = fields.Int(required=True)
    duration = fields.Int(required=False)
    description = fields.Str(required=False)
    genres = fields.List(fields.Str, required=False)
    spotifyEmbed = fields.Str(required=False)
    releaseDate = fields.Str(required=False)


class ArtistSchema(ma.Schema):
    id = fields.Int(required=True)
    name = fields.Str(required=True)
    similarArtists = fields.List(fields.Int, required=False)
    bio = fields.Str(required=False)
    genres = fields.List(fields.Str, required=False)
    lastFmListeners = fields.Int(required=False)
    spotifyFollowers = fields.Int(requried=False)
    image = fields.Str(required=False)
    twitter = fields.Str(required=False)
    age = fields.Int(required=False)
    label = fields.Str(required=False)


class AlbumSchema(ma.Schema):
    id = fields.Int(required=True)
    name = fields.Str(required=True)
    releaseDate = fields.Str(required=False)
    spotifyEmbed = fields.Str(required=False)
    lastFmListeners = fields.Int(required=False)
    description = fields.Str(required=False)
    genres = fields.List(fields.Str, required=False)
    artistId = fields.Int(required=True)
    artistName = fields.Str(required=True)
    songCount = fields.Int(required=True)
    totalDuration = fields.Int(required=False)
    image = fields.Str(required=False)

    def __repr__(self):
        return "<Album %s %s>" % (self.name, self.id)
