from unittest import main, TestCase
import requests
import os
import json

# use env variable to get hostname, if doesn't exist then we use localhost
HOST_NAME = os.getenv("API_HOSTNAME")
if HOST_NAME is None or HOST_NAME == "":
    HOST_NAME = "http://localhost:5000"

print(
    f"\nRunning backend unit test for {HOST_NAME}\n============================================================================\n"
)

# add more tests here by creating a new method
class Tests(TestCase):

    # main page of API loads without error
    def test_main_page_api(self):
        result = requests.get(f"{HOST_NAME}/")
        self.assertEqual(result.status_code, 200)

    # expected number of scraped songs returned by our api
    def test_songs_api(self):
        result = requests.get(f"{HOST_NAME}/songs?limit=30&offset=0")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertEqual(data["totalRecords"], 6178)

    # song instance has correct data, matching expected
    def test_songs_id_api(self):
        result = requests.get(f"{HOST_NAME}/songs/0")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        # validating all data about one song
        self.assertEqual(
            data["song"],
            {
                "albumId": 0,
                "albumName": "Graduation",
                "artistId": 0,
                "artistName": "Kanye West",
                "description": 'On the 10th track from Graduation, \u2018Ye claims his personality is what got him where he is today.\n\nContains samples of \u201cIf We Can\u2019t Be Lovers\u201d by Prince Phillip Mitchell and \u201cBring the Noise\u201d by Public Enemy. <a href="http://www.last.fm/music/Kanye+West/_/Everything+I+Am">Read more on Last.fm</a>.',
                "duration": 227893,
                "genres": ["Hip-Hop", "rap", "Kanye West", "hip hop", "soul"],
                "id": 0,
                "image": "https://lastfm.freetls.fastly.net/i/u/300x300/addee1d378532efe8699f28bc2913fb7.png",
                "lyrics": "[Produced by Kanye West]\n\n[Chorus]\nDamn, here we go again\nCommon passed on this beat, I made it to a jam\nNow everything I'm not made me everything I am\nDamn, here we go again\nPeople talking shit, but when the shit hit the fan\nEverything I'm not made me everything I am\n\n[Verse 1]\nI'll never be picture-perfect Beyonc\u00e9\nBe light as Al B or black as Chauncey\nRemember him from Blackstreet? He was black as the street was\nI'll never be laid back as this beat was\nI never could see why people'll reach a\nFake-ass facade that they couldn't keep up\nYou see how I creeped up?\nYou see how I played a big role in Chicago like Queen Latifah?\nI'll never rock a mink coat in the wintertime like Killa Cam\nOr rock some mink boots in the summertime like will.i.am\nLet me know if you feel it, man\n'Cause everything I'm not made me everything I am\n\n[Chorus]\nDamn, here we go again\nEverybody saying what's not for him\nBut everything I'm not made me everything I am\nDamn, here we go again\nPeople talking shit, but when the shit hit the fan\nEverything I'm not made me everything I am\n\n[Verse 2]\nAnd I'm back to tear it up\nHaters start your engines, I hear 'em gearing up\nPeople talk so much shit about me at barbershops\nThey forget to get their hair cut\nOkay, fair enough, the streets is flaring up\n'Cause they want gun talk, or I don't wear enough\nBaggy clothes, Reeboks, or A-di-dos\nCan I add that he do spaz out at his shows\nSo say goodbye to the NAACP Award\nGoodbye to the India Arie Award\nThey'd rather give me the Ni-Nigga Please Award\nBut I'll just take the I Got a Lot of Cheese Award\n\n[Chorus]\nDamn, here we go again\nBum-tu-bum-bum-bum\nBut everything I'm not made me everything I am\nDamn, here we go again\nPeople talking shit, but when the shit hit the fan\nEverything I'm not made me everything I am\n\n[Verse 3]\nI know people wouldn't usually rap this\nBut I got the facts to back this\nJust last year, Chicago had over six hundred caskets\nMan, killing's some wack shit\nOh, I forgot, 'cept for when niggas is rappin'\nDo you know what it feel like when people is passin'?\nHe got changed over his chains a block off Ashland\nI need to talk to somebody, pastor\nThe church want tithe, so I can't afford to pay\nPink slip on my door, 'cause I can't afford to stay\nMy fifteen seconds up, but I got more to say\nThat's enough Mr. West, please, no more today\n\n[Chorus]\nDamn, here we go again\nEverybody saying what's not for him\nBut everything I'm not made me everything I am\nDamn, here we go again\nPeople talking shit, but when the shit hit the fan\nEverything I'm not made me everything I am",
                "name": "Everything I Am",
                "releaseDate": "2007-09-11",
                "spotifyEmbed": "https://open.spotify.com/embed/track/2T8bL4YC49Ska1yl74qcS0",
            },
        )

    # expected number of scraped artists returned by our api
    def test_artists_api(self):
        result = requests.get(f"{HOST_NAME}/artists?limit=30&offset=0")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertEqual(data["totalRecords"], 187)

    # artists instance has correct data, matching expected
    def test_artists_id_api(self):
        result = requests.get(f"{HOST_NAME}/artists/0")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        # validating all data about one artist
        self.assertEqual(
            data["artist"],
            {
                "age": 44,
                "albums": [
                    {
                        "id": 0,
                        "image": "https://i.scdn.co/image/ab67616d0000b2739bbd79106e510d13a9a5ec33",
                        "name": "Graduation",
                    },
                    {
                        "id": 1,
                        "image": "https://i.scdn.co/image/ab67616d0000b273346d77e155d854735410ed18",
                        "name": "808s & Heartbreak",
                    },
                    {
                        "id": 2,
                        "image": "https://i.scdn.co/image/ab67616d0000b273d9194aa18fa4c9362b47464f",
                        "name": "My Beautiful Dark Twisted Fantasy",
                    },
                ],
                "bio": 'Kanye Omari West (born June 8, 1977 in Atlanta, Georgia), also known as Ye, is a Grammy award-winning American rapper, producer, singer, author, and fashion designer.\n\nWest began making beats and rapping in the early 90s in his hometown of Chicago, Illinois, when he formed the rap group Go Getters with Chicago natives GLC and Really Doe. He later gained nationwide popularity through his work in New York, where he began producing tracks for artists such as Jay-Z, Twista, Mase, Talib Kweli and Alicia Keys. <a href="https://www.last.fm/music/Kanye+West">Read more on Last.fm</a>',
                "genres": ["chicago rap", "rap"],
                "id": 0,
                "image": "https://i.scdn.co/image/ab6761610000e5eb867008a971fae0f4d913f63a",
                "label": "Def Jam Recordings",
                "lastFmListeners": 5147762,
                "name": "Kanye West",
                "similarArtists": [
                    {
                        "id": 10,
                        "image": "https://i.scdn.co/image/ab6761610000e5eb8278b782cbb5a3963db88ada",
                        "name": "Tyler, the Creator",
                    },
                    {
                        "id": 11,
                        "image": "https://i.scdn.co/image/ab6761610000e5ebaf159f008f57546e24846397",
                        "name": "Kendrick Lamar",
                    },
                    {
                        "id": 16,
                        "image": "https://i.scdn.co/image/ab6761610000e5eb981acc715c9138566885a1e7",
                        "name": "Frank Ocean",
                    },
                    {
                        "id": 39,
                        "image": "https://i.scdn.co/image/ab6761610000e5ebd6f2323c1971fd5a70cd0255",
                        "name": "Baby Keem",
                    },
                    {
                        "id": 48,
                        "image": "https://i.scdn.co/image/ab6761610000e5eb876faa285687786c3d314ae0",
                        "name": "Kid Cudi",
                    },
                    {
                        "id": 50,
                        "image": "https://i.scdn.co/image/ab6761610000e5ebec1ef18acc64e5c645c4754c",
                        "name": "Pusha T",
                    },
                ],
                "songs": [
                    {"id": 0, "name": "Everything I Am"},
                    {"id": 1, "name": "Stronger"},
                    {"id": 2, "name": "Can't Tell Me Nothing"},
                    {"id": 3, "name": "Drunk and Hot Girls"},
                    {"id": 4, "name": "The Glory"},
                    {"id": 5, "name": "Good Life"},
                    {"id": 6, "name": "Flashing Lights"},
                    {"id": 7, "name": "Homecoming"},
                    {"id": 8, "name": "Good Morning"},
                    {"id": 9, "name": "I Wonder"},
                    {"id": 10, "name": "Barry Bonds"},
                    {"id": 11, "name": "Champion"},
                    {"id": 12, "name": "Big Brother"},
                    {"id": 13, "name": "RoboCop"},
                    {"id": 14, "name": "Paranoid"},
                    {"id": 15, "name": "Heartless"},
                    {"id": 16, "name": "Street Lights"},
                    {"id": 17, "name": "Say You Will"},
                    {"id": 18, "name": "Coldest Winter"},
                    {"id": 19, "name": "Amazing"},
                    {"id": 20, "name": "Welcome To Heartbreak"},
                    {"id": 21, "name": "See You In My Nightmares"},
                    {"id": 22, "name": "Bad News"},
                    {"id": 23, "name": "Pinocchio Story"},
                    {"id": 24, "name": "Love Lockdown"},
                    {"id": 25, "name": "Gorgeous"},
                    {"id": 26, "name": "Lost In The World"},
                    {"id": 27, "name": "POWER"},
                    {"id": 28, "name": "So Appalled"},
                    {"id": 29, "name": "Runaway"},
                    {"id": 30, "name": "Devil In A New Dress"},
                    {"id": 31, "name": "All Of The Lights"},
                    {"id": 32, "name": "Who Will Survive In America"},
                    {"id": 33, "name": "All Of The Lights (Interlude)"},
                    {"id": 34, "name": "Blame Game"},
                    {"id": 35, "name": "Dark Fantasy"},
                    {"id": 36, "name": "Monster"},
                    {"id": 37, "name": "Hell Of A Life"},
                ],
                "spotifyFollowers": 15253699,
                "twitter": "twitter.com/kanyewest",
            },
        )

    # expected number of scraped albums returned by our api
    def test_albums_api(self):
        result = requests.get(f"{HOST_NAME}/albums?limit=30&offset=0")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertEqual(data["totalRecords"], 498)

    # album instance has correct data, matching expected
    def test_albums_id_api(self):
        result = requests.get(f"{HOST_NAME}/albums/0")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        # validating all data about one album
        self.assertEqual(
            data["album"],
            {
                "artistId": 0,
                "artistName": "Kanye West",
                "description": 'Graduation is the third studio album by American rapper and producer Kanye West, released on September 11, 2007, through Def Jam Recordings and Roc-A-Fella Records. Recording sessions took place between 2005 and 2007 at several studios in New York and Los Angeles. It was primarily produced by West himself, with contributions from various other producers. The album also features guest appearances from recording artists such as Dwele, T-Pain, Lil Wayne, DJ Premier, and Chris Martin of Coldplay. The cover art and its interior artwork were designed by Japanese contemporary artist Takashi Murakami.\n\nInspired by stadium tours, house-music and indie rock, Graduation marked a departure from the ornate, soul-based sound of West\'s previous releases as he musically progressed to more anthemic compositions. West incorporated layered synthesizers and dabbled with electronics while sampling from various music genres and altering his approach to rapping. He conveys an ambivalent outlook on his newfound fame and media scrutiny alongside providing inspirational messages of triumph directed at listeners. Graduation concludes the education theme of West\'s first two albums The College Dropout (2004) and Late Registration (2005).\n\nGraduation debuted at number one on the US Billboard 200, selling over 957,000 copies in the first week of sales. It has since sold over 2.7 million copies in the United States and been certified double platinum by the Recording Industry Association of America (RIAA). Five accompanying singles were released, including the international hits "Stronger", "Good Life" and "Homecoming", the former of which topped the Billboard Hot 100. The album received widely positive reviews from music critics, with several of them praising the production, and earned West his third Grammy Award for Best Rap Album, as well as his third nomination for Album of the Year. It was named as one the best albums of 2007 by multiple publications, including Rolling Stone and USA Today, while also listed among numerous decade-end lists.\n\nThe coinciding release dates between Graduation and rapper 50 Cent\'s Curtis generated much publicity over the idea of a sales competition, resulting in record-breaking sales performances by both albums. The success of the former and the outcome of its competition with the latter marked the end of the dominance of gangsta rap in mainstream hip-hop. This is credited with paving the way for other hip-hop artists who did not conform to gangster conventions to find commercial acceptance.\n\nGraduation is the third installment of West\'s planned tetralogy of education-themed studio albums, which West subsequently later deviated from due to the events surrounding the conception of his fourth studio album, 808s & Heartbreak (2008).\u2019The album demonstrates yet another distinctive progression in West\'s musical style and approach to production. After spending the previous year touring the world with Irish rock band U2 on their Vertigo Tour, West became inspired by watching Bono open the stadium tours every night to incredible ovations and sought out to compose anthemic rap songs that could operate more efficiently in large stadiums and arenas. In West\'s attempt to accomplish this "stadium-status" endeavor, West incorporated layered electronic synthesizers into his hip-hop production, which also finds him utilizing slower tempos, being influenced by the music of the 1980s, and experimenting with electronic music. West was particularly influenced by house music, a subgenre of electronic dance music that first originated in his hometown of Chicago, Illinois in the early 1980s. West has stated that growing up, he would listen to hip-hop music at home or in his car, but when he felt like dancing, he would attend a house club. While he rarely listened to house at home, he still felt it was an important part of his culture and background.\n\nWest further broadened his musical palette on Graduation by not limiting himself to his customary use of samples and interpolation from classic soul records and instead drew influences from a far more eclectic range of music genres. Along with house music, Graduation contains samples and music elements of euro-disco, hard rock, electronica, lounge, progressive rock, synth-pop, electro, krautrock, dub, reggae, and dancehall. Also, for much of the third studio album, West modified his style of rapping and adopted a dilatory, exuberant flow in emulation of Bono\'s operatic singing. West altered his vocabulary, he utilised less percussive consonants for his vocal delivery in favor of smoother vowel harmonies. In addition to U2, West drew inspiration from other arena rock bands such as The Rolling Stones and Led Zeppelin for the melodies and chord progressions of his songs. In terms of lyricism, he simplifies some of his rhymes after touring with The Rolling Stones on their A Bigger Bang concert tour and discovering he could not captivate the audiences as well with his most complex lyrical themes.\n\nWest made a conscious decision to abstain from the widespread recording practice of excessive rap albums saturated with skits and filler and instead comprised Graduation with significantly fewer tracks. He also chose to scale back on the guest appearances, limiting himself to just one single guest rap verse on the entire studio album. West cites the rock bands The Killers, Keane, Modest Mouse, and indie-pop singer-songwriter Feist for being among his favorite musicians and having considerably profound influence on the sound of Graduation. Due largely to these factors and the inclusion of layered electronic synthesizers, West believed that his record took hip-hop in a different direction. He also acknowledged that the differences did not in and of themselves make Graduation a good album; however, he felt it was an accurate representation of the music he was listening to and inspired by at that time.\n\n <a href="https://www.last.fm/music/Kanye+West/Graduation">Read more on Last.fm</a>. User-contributed text is available under the Creative Commons By-SA License; additional terms may apply.',
                "genres": ["hip-hop", "rap", "kanye west", "hip hop", "rnb"],
                "id": 0,
                "image": "https://i.scdn.co/image/ab67616d0000b2739bbd79106e510d13a9a5ec33",
                "lastFmListeners": 1816503,
                "name": "Graduation",
                "releaseDate": "2007-09-11",
                "songCount": 13,
                "songs": [
                    {"id": 0, "name": "Everything I Am"},
                    {"id": 1, "name": "Stronger"},
                    {"id": 2, "name": "Can't Tell Me Nothing"},
                    {"id": 3, "name": "Drunk and Hot Girls"},
                    {"id": 4, "name": "The Glory"},
                    {"id": 5, "name": "Good Life"},
                    {"id": 6, "name": "Flashing Lights"},
                    {"id": 7, "name": "Homecoming"},
                    {"id": 8, "name": "Good Morning"},
                    {"id": 9, "name": "I Wonder"},
                    {"id": 10, "name": "Barry Bonds"},
                    {"id": 11, "name": "Champion"},
                    {"id": 12, "name": "Big Brother"},
                ],
                "spotifyEmbed": "https://open.spotify.com/embed/album/5fPglEDz9YEwRgbLRvhCZy",
                "totalDuration": 3083302,
            },
        )

    # sorting unit tests

    # the first song returned by our api should be the shortest one
    def test_songs_sorting(self):
        result = requests.get(f"{HOST_NAME}/songs?sort=duration&order=ascending&limit=1&offset=0")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertEqual(data["totalRecords"], 6178)
        # validating that all the data for the first song is correct
        self.assertEqual(
          data["page"],
          [{
            "albumId": 102, 
            "albumName": "Because the Internet", 
            "artistId": 36, 
            "artistName": "Childish Gambino", 
            "description": "The Library - Intro is a song created by Childish Gambino with 540 plays in LastFM.", 
            "duration": 4811, 
            "genres": [], 
            "id": 1425, 
            "image": "https://lastfm.freetls.fastly.net/i/u/300x300/2a96cbd8b46e442fc41c2b86b821562f.png", 
            "lyrics": "[Pages of a book being riffled through]", 
            "name": "The Library - Intro", 
            "releaseDate": "2013-12-10", 
            "spotifyEmbed": "https://open.spotify.com/embed/track/45sXSfqfwXxOtknUc9lkjV"
          }],
        )

    # the first artist returned by our api should be the oldest artist
    def test_artists_sorting(self):
        result = requests.get(f"{HOST_NAME}/artists?sort=age&order=descending&limit=1&offset=0")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertEqual(data["totalRecords"], 187)
        # validating that all the data for the first artist is correct
        self.assertEqual(
          data["page"],
          [{
            "age": 81, 
            "bio": "John Ono Lennon, MBE (born John Winston Lennon; 9 October 1940 \u2013 8 December 1980) was an English singer, songwriter, musician, and activist who co-founded The Beatles. He and fellow member Paul McCartney formed a much-celebrated songwriting partnership.\n\nBorn and raised in Liverpool, Lennon became involved in the skiffle craze as a teenager. He formed his first band, the Quarrymen, in 1957 which evolved into the Beatles in 1960. When the group disbanded in 1970 <a href=\"https://www.last.fm/music/John+Lennon\">Read more on Last.fm</a>", 
            "genres": [
              "album rock", 
              "art rock", 
              "beatlesque", 
              "classic rock", 
              "folk rock", 
              "mellow gold", 
              "rock", 
              "soft rock"
            ], 
            "id": 111, 
            "image": "https://i.scdn.co/image/ab6761610000e5eb4ac715292669729d6b640796", 
            "label": "Unknown", 
            "lastFmListeners": 2499533, 
            "name": "John Lennon", 
            "similarArtists": [
              24
            ], 
            "spotifyFollowers": 4422166, 
            "twitter": ""
          }],
        )

    # the first album returned by our api should be the last one alphabetically
    def test_albums_sorting(self):
        result = requests.get(f"{HOST_NAME}/albums?sort=name&order=descending&limit=1&offset=0")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertEqual(data["totalRecords"], 498)
        # validating that all the data for the first album is correct
        self.assertEqual(
          data["page"],
          [{
            "artistId": 42, 
            "artistName": "Glass Animals", 
            "description": "ZABA is an album with 12 songs that was created by Glass Animals with 368,460 plays in LastFM.", 
            "genres": [
              "2014", 
              "best of 2014", 
              "indie", 
              "digitalis", 
              "minimal pop"
            ], 
            "id": 120, 
            "image": "https://i.scdn.co/image/ab67616d0000b2737a293e6787c6d200c5077cd0", 
            "lastFmListeners": 368460, 
            "name": "ZABA", 
            "releaseDate": "2014-06-03", 
            "songCount": 11, 
            "spotifyEmbed": "https://open.spotify.com/embed/album/14IOe7ahxQPTwUYUQX3IFi", 
            "totalDuration": 2737956
          }],
        )

    # filtering unit tests

    # testing a specific song filter
    def test_songs_filtering(self):
        result = requests.get(f"{HOST_NAME}/songs?sort=duration&order=ascending&filterName=Shake&filterDuration=219000,220000")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertEqual(data["totalRecords"], 1)
        # validating that all the data for the song is correct
        self.assertEqual(
          data["page"],
          [{
            "albumId": 20, 
            "albumName": "1989", 
            "artistId": 6, 
            "artistName": "Taylor Swift", 
            "description": "\"Shake It Off\" is a song recorded by American singer-songwriter Taylor Swift for her fifth studio album 1989, marking a rapid departure from her signature country pop musical style in favor of a more \"radio-friendly\" format. The song was released as the lead single from the album on August 18, 2014, by Big Machine Records. An accompanying music video directed by Mark Romanek was released on the same day as the song's release. <a href=\"http://www.last.fm/music/Taylor+Swift/_/Shake+It+Off\">Read more on Last.fm</a>.", 
            "duration": 219200, 
            "genres": [
              "pop",
              "haters gonna hate",
              "2014",
              "taylor swift",
              "love at first listen"
            ], 
            "id": 259, 
            "image": "https://lastfm.freetls.fastly.net/i/u/300x300/b71ba6cf5881d8cf143e0ad0b4f28d9f.png", 
            "lyrics": "[Verse 1]\nI stay out too late\nGot nothing in my brain\nThat's what people say, mmm-mmm\nThat's what people say, mmm-mmm\nI go on too many dates\nBut I can't make them stay\nAt least that's what people say, mmm-mmm\nThat's what people say, mmm-mmm\n\n[Pre-Chorus]\nBut I keep cruisin'\nCan't stop, won't stop movin'\nIt's like I got this music in my mind\nSayin' it's gonna be alright\n\n[Chorus]\n'Cause the players gonna play, play, play, play, play\nAnd the haters gonna hate, hate, hate, hate, hate\nBaby, I'm just gonna shake, shake, shake, shake, shake\nI shake it off, I shake it off\nHeartbreakers gonna break, break, break, break, break\nAnd the fakers gonna fake, fake, fake, fake, fake\nBaby, I'm just gonna shake, shake, shake, shake, shake\nI shake it off, I shake it off\n\n[Verse 2]\nI never miss a beat\nI'm lightnin' on my feet\nAnd that's what they don't see, mmm-mmm\nThat's what they don't see, mmm-mmm\nI'm dancin' on my own (Dancin' on my own)\nI make the moves up as I go (Moves up as I go)\nAnd that's what they don't know, mmm-mmm\nThat's what they don't know, mmm-mmm\n\n[Pre-Chorus]\nBut I keep cruisin'\nCan't stop, won't stop groovin'\nIt's like I got this music in my mind\nSayin' it's gonna be alright\n\n[Chorus]\n'Cause the players gonna play, play, play, play, play\nAnd the haters gonna hate, hate, hate, hate, hate\nBaby, I'm just gonna shake, shake, shake, shake, shake\nI shake it off, I shake it off\nHeartbreakers gonna break, break, break, break, break\nAnd the fakers gonna fake, fake, fake, fake, fake\nBaby, I'm just gonna shake, shake, shake, shake, shake\nI shake it off, I shake it off\n\n[Post-Chorus]\nShake it off, I shake it off\nI, I, I shake it off, I shake it off\nI, I, I shake it off, I shake it off\nI, I, I shake it off, I shake it off\n\n[Interlude]\nHey, hey, hey\nJust think, while you've been gettin' down and out about the liars\nAnd the dirty, dirty cheats of the world\nYou could've been gettin' down\nTo this sick beat\n\n[Bridge]\nMy ex-man brought his new girlfriend\nShe's like, \"Oh my God!\" But I'm just gonna shake\nAnd to the fella over there with the hella good hair\nWon't you come on over, baby?\nWe can shake, shake, shake\nYeah, oh, oh, oh\n\n[Chorus]\n'Cause the players gonna play, play, play, play, play\nAnd the haters gonna hate, hate, hate, hate, hate\n(Haters gonna hate)\nBaby, I'm just gonna shake, shake, shake, shake, shake\nI shake it off, I shake it off\nHeartbreakers gonna break, break, break, break, break (Mmm)\nAnd the fakers gonna fake, fake, fake, fake, fake\n(And fake, and fake, and fake)\nBaby, I'm just gonna shake, shake, shake, shake, shake\nI shake it off, I shake it off\n\n[Post-Chorus]\nShake it off, I shake it off\nI, I, I shake it off, I shake it off\nI, I, I shake it off, I shake it off\nI, I, I shake it off, I shake it off\nShake it off, I shake it off\nI, I, I shake it off, I shake it off\nI, I, I shake it off, I shake it off\nI, I, I shake it off, I shake it off (Yeah)\nShake it off, I shake it off\nI, I, I shake it off, I shake it off (You got to)\nI, I, I shake it off, I shake it off\nI, I, I shake it off, I shake it off\n\n[Video directed by Mark Romanek]", 
            "name": "Shake It Off", 
            "releaseDate": "2014-10-27", 
            "spotifyEmbed": "https://open.spotify.com/embed/track/5xTtaWoae3wi06K5WfVUUH"
          }],
        )

    # testing one artists filter to make sure it returns the correct number of results
    def test_artists_filtering(self):
        result = requests.get(f"{HOST_NAME}/artists?filterName=T")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertEqual(data["totalRecords"], 17)

    # testing one albums filter (but with multiple inputs), to make sure it returns the correct number of results
    def test_albums_filtering(self):
        result = requests.get(f"{HOST_NAME}/albums?filterSongCount=13,14")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertEqual(data["totalRecords"], 40)

    # searching unit tests

    # queries the songs page to see if the correct number of relevant results was returned
    def test_songs_search(self):
        result = requests.get(f"{HOST_NAME}/songs?search=yuh")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertEqual(data["totalRecords"], 46)

    # queries the artists page to see if the correct number of relevant results was returned
    def test_artists_search(self):
        result = requests.get(f"{HOST_NAME}/artists?search=taylor")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertEqual(data["totalRecords"], 9)

    # queries the albums page to see if the correct number of relevant results was returned
    def test_albums_search(self):
        result = requests.get(f"{HOST_NAME}/albums?search=rap")
        self.assertEqual(result.status_code, 200)
        data = result.json()
        self.assertEqual(data["totalRecords"], 144)

    # queries the entire website to see if the correct number of aggregate
    # relevant song, artist, and album instances were returned
    # for now, a site wide search searches all three models with the query and aggregates the results
    # it's up to the frontend to resolve duplicate instances that could be returned
    def test_sitewide_search(self):
        song_result = requests.get(f"{HOST_NAME}/songs?search=kanye west")
        self.assertEqual(song_result.status_code, 200)
        song_data = song_result.json()
        self.assertEqual(song_data["totalRecords"], 190)
        num_songs = song_data["totalRecords"]

        artist_result = requests.get(f"{HOST_NAME}/artists?search=kanye west")
        self.assertEqual(artist_result.status_code, 200)
        artist_data = artist_result.json()
        self.assertEqual(artist_data["totalRecords"], 5)
        num_artists = artist_data["totalRecords"]

        album_result = requests.get(f"{HOST_NAME}/albums?search=kanye west")
        self.assertEqual(album_result.status_code, 200)
        album_data = album_result.json()
        self.assertEqual(album_data["totalRecords"], 10)
        num_albums = album_data["totalRecords"]

        total_search_results = num_songs + num_artists + num_albums
        self.assertEqual(total_search_results, 205)

if __name__ == "__main__":
    main()
