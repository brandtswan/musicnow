from flask import Flask, jsonify, request
import sqlalchemy
from flask_cors import CORS
from models import db, app, Song, Artist, Album, SongSchema, ArtistSchema, AlbumSchema
from sqlalchemy import or_


# format based off TexasVotes https://gitlab.com/forbesye/fitsbits/-/blob/master/back-end/app.py

# wrapper function to send a json response with cors
def send(res):
    response = jsonify(res)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


# helper function to search for specific columns for a model and return a query
def process_search(
    model, searchable_text_columns, searchable_array_columns, search_term
):
    all_model = None
    if search_term:
        query = []

        for column in searchable_text_columns:
            query.append(column.ilike("%" + search_term + "%"))
        for column in searchable_array_columns:
            query.append(column.any(search_term))

        all_model = db.session.query(model).filter(or_(*query))
    else:
        all_model = db.session.query(model)

    return all_model


# helper function to sort by a single column for a model
def process_sort(model, sort, direction):
    if direction == "descending":
        sorting_expression = sqlalchemy.sql.expression.desc(sort)
    else:
        sorting_expression = sqlalchemy.sql.expression.asc(sort)
    return model.order_by(sorting_expression)


# helper function to filter a column by all applicable filters
def process_filter(model, filterable_columns, filters):
    for i in range(0, len(filters)):
        d = filters[i]
        filter = list(d)[0]
        if filter != None:
            if d[filter] == "name":
                model = model.filter(filterable_columns[i].like(str(filter) + "%"))
            elif d[filter] == "range":
                startRange = filter[: filter.index(",")]
                endRange = filter[filter.index(",") + 1 :]
                model = model.filter(
                    filterable_columns[i] >= int(startRange),
                    filterable_columns[i] < int(endRange),
                )
            elif d[filter] == "match":
                model = model.filter(filterable_columns[i] == filter)
            elif d[filter] == "date":
                model = model.filter(
                    filterable_columns[i].between(
                        str(filter) + "-01-01", str(filter) + "-12-31"
                    )
                )

    return model


def parse_song_filters(query):
    nameFilter = query.get("filterName")
    artistFilter = query.get("filterArtist")
    albumFilter = query.get("filterAlbumName")
    durationFilter = query.get("filterDuration")
    dateFilter = query.get("filterReleaseDate")
    return {
        "name": nameFilter,
        "artist": artistFilter,
        "album": albumFilter,
        "duration": durationFilter,
        "date": dateFilter,
    }


def parse_artist_filters(query):
    nameFilter = query.get("filterName")
    labelFilter = query.get("filterLabel")
    ageFilter = query.get("filterAge")
    listenersFilter = query.get("filterLastFMListeners")
    followersFilter = query.get("filterSpotifyFollowers")
    return {
        "name": nameFilter,
        "label": labelFilter,
        "age": ageFilter,
        "listeners": listenersFilter,
        "followers": followersFilter,
    }


def parse_album_filters(query):
    nameFilter = query.get("filterName")
    artistFilter = query.get("filterArtist")
    songCountFilter = query.get("filterSongCount")
    durationFilter = query.get("filterDuration")
    dateFilter = query.get("filterReleaseDate")
    listenersFilter = query.get("filterLastFMListeners")
    return {
        "name": nameFilter,
        "artist": artistFilter,
        "songCount": songCountFilter,
        "duration": durationFilter,
        "date": dateFilter,
        "listeners": listenersFilter,
    }


@app.route("/")
def main_page():
    return '<img src="https://m.media-amazon.com/images/I/71aPyHrRGkL._SY500_.jpg" alt=":)" />'


@app.route("/songs", methods=["GET"])
def songs():
    limit = request.args.get("limit")
    offset = request.args.get("offset")

    search_term = request.args.get("search")
    sort_term = request.args.get("sort")
    sort_direction = request.args.get("order")
    filters = parse_song_filters(request.args)

    if sort_term == "name":
        sort = Song.name
    elif sort_term == "artist":
        sort = Song.artistName
    elif sort_term == "album_name":
        sort = Song.albumName
    elif sort_term == "duration":
        sort = Song.duration
    else:
        sort = Song.releaseDate

    all_songs = process_search(
        Song,
        [Song.name, Song.albumName, Song.artistName, Song.lyrics, Song.description],
        [Song.genres],
        search_term,
    )
    sorted_songs = process_sort(all_songs, sort, sort_direction)
    filtered_songs = process_filter(
        sorted_songs,
        [Song.name, Song.releaseDate, Song.duration, Song.albumName, Song.artistName],
        [
            {filters["name"]: "name"},
            {filters["date"]: "date"},
            {filters["duration"]: "range"},
            {filters["album"]: "name"},
            {filters["artist"]: "name"},
        ],
    )
    total_records = filtered_songs.count()
    filtered_songs = filtered_songs.limit(limit).offset(offset)
    filtered_songs = SongSchema().dump(filtered_songs, many=True)

    return send({"page": filtered_songs, "totalRecords": total_records})


@app.route("/songs/<int:id>", methods=["GET"])
def song_instance(id):
    songInstance = db.session.query(Song).filter_by(id=id)
    songInstance = SongSchema().dump(songInstance, many=True)[0]
    return send({"song": songInstance})


@app.route("/artists", methods=["GET"])
def artists():
    limit = request.args.get("limit")
    offset = request.args.get("offset")

    search_term = request.args.get("search")
    sort_term = request.args.get("sort")
    sort_direction = request.args.get("order")
    filters = parse_artist_filters(request.args)

    if sort_term == "name":
        sort = Artist.name
    elif sort_term == "label":
        sort = Artist.label
    elif sort_term == "age":
        sort = Artist.age
    elif sort_term == "last_fm_listeners":
        sort = Artist.lastFmListeners
    else:
        sort = Artist.spotifyFollowers

    all_artists = process_search(
        Artist, [Artist.name, Artist.bio, Artist.label], [], search_term
    )
    sorted_artists = process_sort(all_artists, sort, sort_direction)
    filtered_artists = process_filter(
        sorted_artists,
        [
            Artist.name,
            Artist.label,
            Artist.age,
            Artist.lastFmListeners,
            Artist.spotifyFollowers,
        ],
        [
            {filters["name"]: "name"},
            {filters["label"]: "name"},
            {filters["age"]: "range"},
            {filters["listeners"]: "range"},
            {filters["followers"]: "range"},
        ],
    )

    total_records = filtered_artists.count()

    filtered_artists = filtered_artists.limit(limit).offset(offset)
    filtered_artists = ArtistSchema().dump(filtered_artists, many=True)

    return send({"page": filtered_artists, "totalRecords": total_records})


@app.route("/artists/<int:id>", methods=["GET"])
def artist_instance(id):
    artistInstance = db.session.query(Artist).filter_by(id=id)
    artistInstance = ArtistSchema().dump(artistInstance, many=True)[0]

    similarArtists = db.session.query(Artist.image, Artist.id, Artist.name).filter(
        Artist.id.in_(artistInstance["similarArtists"])
    )
    similarArtists = ArtistSchema().dump(similarArtists, many=True)
    artistInstance["similarArtists"] = similarArtists

    albums = db.session.query(Album.image, Album.id, Album.name).filter(
        Album.artistId == id
    )
    albums = AlbumSchema().dump(albums, many=True)
    artistInstance["albums"] = albums

    songs = db.session.query(Song.name, Song.id).filter(Song.artistId == id)
    songs = SongSchema().dump(songs, many=True)
    artistInstance["songs"] = songs

    return send({"artist": artistInstance})


@app.route("/albums", methods=["GET"])
def albums():
    limit = request.args.get("limit")
    offset = request.args.get("offset")

    search_term = request.args.get("search")
    sort_term = request.args.get("sort")
    sort_direction = request.args.get("order")
    filters = parse_album_filters(request.args)

    if sort_term == "name":
        sort = Album.name
    elif sort_term == "artist_name":
        sort = Album.artistName
    elif sort_term == "song_count":
        sort = Album.songCount
    elif sort_term == "total_duration":
        sort = Album.totalDuration
    elif sort_term == "last_fm_listeners":
        sort = Album.lastFmListeners
    else:
        sort = Album.releaseDate

    all_albums = process_search(
        Album,
        [Album.artistName, Album.description, Album.name],
        [Album.genres],
        search_term,
    )
    sorted_albums = process_sort(all_albums, sort, sort_direction)
    filtered_albums = process_filter(
        sorted_albums,
        [
            Album.name,
            Album.artistName,
            Album.songCount,
            Album.totalDuration,
            Album.lastFmListeners,
            Album.releaseDate,
        ],
        [
            {filters["name"]: "name"},
            {filters["artist"]: "name"},
            {filters["songCount"]: "range"},
            {filters["duration"]: "range"},
            {filters["listeners"]: "range"},
            {filters["date"]: "range"},
        ],
    )

    total_records = filtered_albums.count()

    filtered_albums = filtered_albums.limit(limit).offset(offset)
    filtered_albums = AlbumSchema().dump(filtered_albums, many=True)

    return send({"page": filtered_albums, "totalRecords": total_records})


@app.route("/albums/<int:id>", methods=["GET"])
def album_instance(id):
    albumInstance = db.session.query(Album).filter_by(id=id)
    albumInstance = AlbumSchema().dump(albumInstance, many=True)[0]

    songs = db.session.query(Song.name, Song.id).filter(Song.albumId == id)
    songs = SongSchema().dump(songs, many=True)
    albumInstance["songs"] = songs

    return send({"album": albumInstance})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
