# Image with both python and node installed
FROM nikolaik/python-nodejs:python3.7-nodejs14

# Copying files over to a work directory
COPY . /app
RUN ls
WORKDIR /app

# setting up the frontend (installing packages)
RUN make setup-frontend

# setting up the backend (installing packages)
# have to explicitly copy requirements.txt so docker is able to use it during image building
COPY /backend/requirements.txt /app/backend/requirements.txt
RUN make setup-backend

# starting up a shell to start developing in
ENTRYPOINT [ "/bin/bash" ]
